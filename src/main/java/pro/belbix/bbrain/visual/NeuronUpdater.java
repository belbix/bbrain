package pro.belbix.bbrain.visual;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Sphere;
import org.apache.kafka.common.protocol.types.Field;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.neuron.*;
import pro.belbix.bbrain.utils.Config;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static pro.belbix.bbrain.utils.Config.*;

/**
 * Created by v.belykh on 13.07.2018
 */
public class NeuronUpdater {
    private static final Logger log = LogManager.getRootLogger();
    private BitmapFont font;
    private Material defaultMaterial;
    private Map<String, Spatial> global;
    private Node node;
    private Vector3f centerPoint;
    private static final String TEXT_PREFIX = "text";
    private static final String INPUT_PREFIX = "input";

    public NeuronUpdater(Node node, Map<String, Spatial> global, AssetManager assetManager) {
        this.global = global;
        this.node = node;

        this.font = assetManager.loadFont("Interface/Fonts/Default.fnt");
        this.defaultMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    }

    public void updateNeuronNode(Neuron neuron) {
        try {
            centerPoint = new Vector3f(neuron.getX() * Config.widthNeuron
                    , neuron.getY() * Config.heightLayer
                    , neuron.getZ() * Config.widthNeuron);

            clearDeleted(neuron);

            //--------------------NEURON BODY------------------------------
            updateNeuronBody(neuron);

            //--------------------DENDRITE------------------------------
            updateDendrites(neuron);

            //--------------------AXON------------------------------
            updateAxons(neuron);
        } catch (Exception e) {
            log.error("Error update " + neuron, e);
        }
    }

    private Vector3f createPoint(Neuron neuron) {
        long lCount = neuron.getLayer_id();
        long nId = neuron.getId();
        float median = 0;//(maxLayerCount - lCount) / 2;
        float xStart = 0;// median * Config.widthNeuron;
        if (Config.zLines) xStart = FastMath.sqrt(xStart);

        Layer layer = neuron.getLayer();
        if (layer == null) {
            log.warn("null layer " + neuron);
            return null;
        }
        float line = FastMath.floor(FastMath.sqrt(layer.getNeuronCount()));

        float nnZ = FastMath.floor(nId / line);
        long nnId = nId;
        if (Config.zLines) nnId = nId - (long) (nnZ * line);


        float x = xStart + (nnId * Config.widthNeuron);
        float y = (neuron.getLayer_id() * Config.heightLayer);
        float z = 0;
        if (Config.zLines) z = nnZ * Config.widthNeuron;
        return new Vector3f(x, y, z);
    }

    private void clearDeleted(Neuron neuron){
        while (true) {
            String name = neuron.getDeleted().poll();
            if(name == null) break;
            node.detachChildNamed(name);
            node.detachChildNamed(TEXT_PREFIX + name);
//            node.detachChildNamed(TEXT_PREFIX + INPUT_PREFIX + name);
            global.remove(name);
        }
    }

    private void updateAxons(Neuron neuron) {
        List<Axon> axons = neuron.getAxons();
        float xEnd = centerPoint.getX();
        float xPlus = widthAxon;
        if (axons.size() > 1) {
            xEnd = centerPoint.getX() - (widthAxon / 2);
            xPlus = widthAxon / ((float) (axons.size() - 1));
        }

        for (Axon axon : axons) {
            Geometry geo = (Geometry) node.getChild(axon.getName());
            if (geo != null) node.detachChild(geo);

            Vector3f end = new Vector3f(xEnd, centerPoint.getY() + heightAxon, centerPoint.getZ());
            Line line = new Line(centerPoint, end);
            geo = new Geometry(axon.getName(), line);
            Material materialClone = defaultMaterial.clone();
            materialClone.setColor("Color", ColorRGBA.Gray);
            geo.setMaterial(materialClone);
            node.attachChild(geo);

            addText(node,
                    end.add(-1f, 2f, 1f),
                    axon.getName(),
                    VisualUtils.createAxonText(axon),
                    0.3f,
                    ColorRGBA.Green);

            updateSenders(axon, end);
            axon.setWasActivate(false);
            xEnd += xPlus;
        }
    }

    private Vector3f getSenderDestination(Sender sender, long axonId) {
        Vector3f end = null;

        if (sender.getAxon_id() != axonId
                || sender.getReceiver_id() == null
                || sender.getRlayer_id() == null
                || sender.getRneuron_id() == null
                || sender.getRdendrite_id() == null) return null;

        String rName = Receiver.createName(sender.getRlayer_id(),
                sender.getRneuron_id(),
                sender.getRdendrite_id(),
                sender.getReceiver_id());
        Geometry receiverSpatial = (Geometry) global.get(rName);
        if (receiverSpatial != null) {
            Line line = (Line) receiverSpatial.getMesh();
            end = line.getEnd();
        }

        return end;
    }

    private void updateSenders(Axon axon, Vector3f point) {
        List<Sender> senders = axon.getSenders();
        long axonId = axon.getId();
        for (Sender sender : senders) {
            Vector3f end = getSenderDestination(sender, axonId);
            if (end == null) continue;

            int power;
            Geometry geo = (Geometry) node.getChild(sender.getName());
            if (geo != null) {
                node.detachChild(geo);
            }
            Vector3f start = new Vector3f(point);
            Line line = new Line(start, end);
            geo = new Geometry(sender.getName(), line);
            power = getPower(sender);
            node.attachChild(geo);
            boolean activated;
            if (sender.getActivated().getValue() != null)
                activated = sender.getActivated().getValue();
            else
                activated = false;
            setActiveMaterial(geo, activated, power);
            sender.getActivated().setValue(false);
        }
    }

    private int getPower(Sender sender) {
        int power = 0;
        for (Transmitter transmitter : sender.getTransmitters()) {
            if (!Objects.equals(transmitter.getSender_id(), sender.getId())) continue;
            switch (transmitter.getSynapse().getType()) {
                case ACTIVATOR:
                    power++;
                    break;
                case INHIBITOR:
                    power--;
                    break;
            }
            break;
        }
        return power;
    }

    private void setActiveMaterial(Geometry geo, boolean active, int power) {
        float innactiveK = 3;
        Material material = defaultMaterial.clone();
        if(power > 0){
            if (active)
                material.setColor("Color", new ColorRGBA(1, 0.5f, 0.5f, 0));
            else
                material.setColor("Color", new ColorRGBA(1 / innactiveK, 0.5f / innactiveK, 0.5f / innactiveK, 0));
        } else if(power < 0){
            if (active)
                material.setColor("Color", new ColorRGBA(0.5f, 0.5f, 1, 0));
            else
                material.setColor("Color", new ColorRGBA(0.5f / innactiveK, 0.5f / innactiveK, 1 / innactiveK, 0));
        } else {
            if (active) {
                material.setColor("Color", new ColorRGBA(0.5f, 1, 0.5f, 0));
            } else {
                material.setColor("Color", new ColorRGBA(0.5f / innactiveK, 1 / innactiveK, 0.5f / innactiveK, 0));
            }
        }
        geo.setMaterial(material);
    }

    private void updateDendrites(Neuron neuron) {
        List<Dendrite> dendrites = neuron.getDendrits();
        float xEnd = centerPoint.getX();
        float xPlus = widthDendrite;
        if (dendrites.size() > 1) {
            xEnd = centerPoint.getX() - (widthDendrite / 2);
            xPlus = widthDendrite / ((float) (dendrites.size() - 1));
        }

        for (Dendrite dendrite : dendrites) {
            Vector3f end = new Vector3f(xEnd, centerPoint.getY() - heightDendrite, centerPoint.getZ());
            Geometry geo = (Geometry) node.getChild(dendrite.getName());
            if (geo != null) node.detachChild(geo);
            geo = createLine(centerPoint, end, dendrite.getName());
            node.attachChild(geo);

            addText(node,
                    end.add(-1f, 1f, 1f),
                    dendrite.getName(),
                    VisualUtils.createDendriteText(dendrite),
                    0.3f,
                    ColorRGBA.Green);

            updateReceivers(dendrite, end, xPlus * 0.5f);

            xEnd += xPlus;
        }
    }

    private void updateReceivers(Dendrite dendrite, Vector3f point, float width) {
        long dendriteId = dendrite.getId();
        List<Receiver> receivers = dendrite.getReceivers();
        if (receivers == null || receivers.isEmpty()) return;
        int receiversCount = 0;
        for (Receiver model : receivers) {
            if (model.getDendrite_id() != dendriteId) continue;
            receiversCount++;
        }

        float xEnd = point.getX();
        float xPlus = width;
        if (receivers.size() > 1) {
            xEnd = point.getX() - (width / 2);
            xPlus = width / ((float) (receiversCount - 1));
        }

        for (Receiver receiver : receivers) {
            if (receiver.getDendrite_id() != dendriteId) continue;
            Vector3f end = new Vector3f(xEnd, point.getY() - heightReceiver, point.getZ());
            Geometry geo = (Geometry) node.getChild(receiver.getName());
            if (geo != null) node.detachChild(geo);
            geo = createLine(point, end, receiver.getName());
            node.attachChild(geo);
            global.put(receiver.getName(), geo);

            updateReceptors(receiver, end);
            xEnd += xPlus;
        }
    }

    private Geometry createLine(Vector3f start, Vector3f end, String name) {
        Line line = new Line(start, end);
        Geometry geo = new Geometry(name, line);
        Material materialClone = defaultMaterial.clone();
        materialClone.setColor("Color", ColorRGBA.Gray);
        geo.setMaterial(materialClone);
        return geo;
    }

    private void updateReceptors(Receiver receiver, Vector3f point) {
        double activity = receiver.getActivity();
        List<Receptor> receptors = receiver.getReceptors();
        if (receptors == null) return;
        for (Receptor receptor : receptors) {
            Geometry geo = (Geometry) node.getChild(receptor.getName());
            if (geo != null) node.detachChild(geo);

            Sphere sphereMesh = new Sphere(5, 5, 0.1f);
            geo = new Geometry(receptor.getName(), sphereMesh);
            Material materialClone = defaultMaterial.clone();
            materialClone.setColor("Color", ColorRGBA.Gray);
            geo.setMaterial(materialClone);
            geo.setLocalTranslation(point);
            node.attachChild(geo);
            setColor(geo, createColorFromActivity((float) activity));
        }
    }

    private void updateNeuronBody(Neuron neuron) {
        //---------------- BODY ---------------------------
        Geometry geo = (Geometry) node.getChild(neuron.getName());
        if (geo != null) node.detachChild(geo);
        Sphere sphereMesh = new Sphere(8, 8, 1f);
        geo = new Geometry(neuron.getName(), sphereMesh);
        geo.setLocalTranslation(centerPoint);
        setColor(geo, createColorFromEP(neuron));
        node.attachChild(geo);

        //---------------- TEXT ---------------------------
        addText(node,
                centerPoint.add(new Vector3f(1f, 1f, 1f)),
                neuron.getName(),
                VisualUtils.createNeuronText(neuron),
                0.5f,
                ColorRGBA.Green);

        //---------------- INPUT ---------------------------
        if (neuron.getInputInfo() != null && neuron.getInputInfo().getValue() != null) {
            addText(node,
                    centerPoint.add(new Vector3f(0f, -2f, 1f)),
                    INPUT_PREFIX + neuron.getName(),
                    neuron.getInputInfo().getValue(),
                    0.3f,
                    ColorRGBA.Orange);
        }

        neuron.getLastAmountPotentials().setValue(0L);
        neuron.getInputInfo().setValue("");
    }

    private void addText(Node node, Vector3f textPoint,String elementName , String text, float size, ColorRGBA color) {
        String textName = TEXT_PREFIX + elementName;
        BitmapText bitmapText = (BitmapText) node.getChild(textName);
        if (bitmapText != null) node.detachChild(bitmapText);
        bitmapText = new BitmapText(font);
        bitmapText.setSize(size);
        bitmapText.setColor(color);
        bitmapText.setLocalTranslation(textPoint);
        bitmapText.setName(textName);
        bitmapText.setText(text);
        node.attachChild(bitmapText);

    }

    private void setColor(Geometry geo, ColorRGBA rgba) {
        Material materialClone = defaultMaterial.clone();
        materialClone.setColor("Color", rgba);
        geo.setMaterial(materialClone);
    }

    private ColorRGBA createColorFromActivity(float activity) {
        float r = activity / ((float) Receiver.MAX_LAST_PROCESSING);
        if (r > 1) r = 1;
        return new ColorRGBA(1, (1 - r), (1 - r), 0);
    }

    private ColorRGBA createColorFromEP(Neuron neuron) {
        Long ap = neuron.getLastAmountPotentials().getValue();
        if (ap == null) ap = 0L;
        Long refractoryResist = neuron.getRefractoryResist();
        if (refractoryResist == null) refractoryResist = 0L;
        float resist = refractoryResist + neuron.getMembraneResistance();
        float r = (ap / resist);
        if (ap > 0) {
            if (r > 1) r = 1;
            return new ColorRGBA(1, (1 - r), (1 - r), 0);
        } else {
            r = -r;
            if (r > 1) r = 1;
            return new ColorRGBA((1 - r), (1 - r), 1, 0);
        }
    }
}
