package pro.belbix.bbrain.visual;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.models.Key;
import pro.belbix.bbrain.neuron.Neuron;
import pro.belbix.bbrain.processing.ProcessingCenter;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by v.belykh on 9/20/2018
 */
public class MessageReceiver implements Callable<Integer> {
    private static final Logger log = LogManager.getRootLogger();
    private static final int MAX_COUNT = 1000;
    private Node rootNode = null;
    private ConcurrentHashMap<Key, Node> nodes = null;
    private ConcurrentHashMap<String, Spatial> globalSpatial = null;
    private AssetManager assetManager;
    private int count = 0;
    private int countSkip = 0;

    public MessageReceiver(Node rootNode,
                           ConcurrentHashMap<Key, Node> nodes,
                           ConcurrentHashMap<String, Spatial> globalSpatial,
                           AssetManager assetManager) {
        this.rootNode = rootNode;
        this.nodes = nodes;
        this.globalSpatial = globalSpatial;
        this.assetManager = assetManager;
    }

    @Override
    public Integer call() {
        LocalDateTime start = LocalDateTime.now();
        count = 0;
        countSkip = 0;

        while (count < MAX_COUNT) {
            Neuron neuron = ProcessingCenter.getNeuronForVisual().poll();
            if (neuron == null) break;
            synchronized (neuron.lock) {
                if (
                        !neuron.isReadyToShow() ||
                                BBVisual.isNeuronInProcessing(neuron)) {
                    countSkip++;
                    continue;
                }

                count++;
                Node node = nodes.get(neuron.getKey());
                if (node == null) {
                    node = new Node();
                    rootNode.attachChild(node);
                    nodes.put(neuron.getKey(), node);
                }
                NeuronUpdater neuronUpdater = new NeuronUpdater(node, globalSpatial, assetManager);
                neuronUpdater.updateNeuronNode(neuron);
                BBVisual.getNeurons().remove(neuron.getKey());
                neuron.setLastVisualization(Instant.now());
            }
        }
        if (count + countSkip > 0) {
            log.debug("update " + count + " (skip " + countSkip + ") neurons of(" + nodes.size() + ") by time : "
                    + Duration.between(start, LocalDateTime.now()));
        }
        return count + countSkip;
    }
}
