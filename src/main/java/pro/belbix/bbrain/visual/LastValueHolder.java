package pro.belbix.bbrain.visual;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class LastValueHolder<T> {
    private T value;
    private Instant lastTime;
    private int pause;

    public LastValueHolder(int pause) {
        this.pause = pause;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        if (lastTime != null) {
            Duration duration = Duration.between(lastTime, Instant.now());
            Duration p = Duration.of(pause, ChronoUnit.MILLIS);
            if (duration.minus(p).isNegative())
                return;
        }
        setValueInstantly(value);
    }

    public void setValueInstantly(T value){
        this.value = value;
        this.lastTime = Instant.now();
    }
}
