package pro.belbix.bbrain.visual;

import pro.belbix.bbrain.neuron.*;

/**
 * Created by v.belykh on 10/9/2018
 */
public class VisualUtils {

    public static String createNeuronText(Neuron neuron) {
        StringBuilder sb = new StringBuilder();
        try {
            addLine(sb, null, neuron.getName());
            addLine(sb, null, neuron.getLastAmountPotentials().getValue() +
                    "/" + (neuron.getRefractoryResist() + neuron.getMembraneResistance()) + "(" + neuron.isAbsoluteRefractory() + ")"
            );
            addLine(sb, "w:", neuron.getWishDendrite() + ";a:" + neuron.getActivity());
        } catch (Exception e) {
            sb.append(" error ").append(e.getMessage());
        }
        return sb.toString();
    }

    public static String createDendriteText(Dendrite dendrite) {
        StringBuilder sb = new StringBuilder();
        try {
            addLine(sb, "d" + dendrite.getId() + ":", dendrite.getWishReceiver() + ";" + dendrite.getActivity());
            for (Receiver receiver : dendrite.getReceivers()) {
                addLine(sb, "  rv" + receiver.getId() + ":", receiver.getWishSender() +
                        ";" + receiver.getDeath() +
                        ";" + Math.round(receiver.getActivity()) +
                        ";" + (receiver.getLastActivity() == null ? "-" : "+")
                );
                for (Receptor receptor : receiver.getReceptors()) {
                    addLine(sb, "    r" + receptor.getId() + ":",
                            Math.round(receptor.getEnergy()) +
                                    ";" + receptor.getSynapse().getType().name().substring(0, 1));
                }
            }
        } catch (Exception e) {
            sb.append(" error ").append(e.getMessage());
        }
        return sb.toString();
    }

    public static String createAxonText(Axon axon) {
        StringBuilder sb = new StringBuilder();
        try {
            addLine(sb, "a" + axon.getId() + ":", axon.getWishCreateSender() + "");
            for (Sender sender : axon.getSenders()) {
                addLine(sb, "  s" + sender.getId() + ":",
                        sender.getWishGetReceiver() + ";" +
                                sender.getPotentiationResource() + ";" +
                                sender.getPotentiationBalance() + ";"
                );
                for (Transmitter transmitter : sender.getTransmitters()) {
                    addLine(sb, "    t" + transmitter.getId() + ":",
                            transmitter.getCount() + ";" +
                                    transmitter.getModifyReceptorPowerValue() + ";" +
                                    transmitter.getNeedToActivation() + ";" +
                                    transmitter.getSynapse().getType().name().substring(0, 1));

                }
            }
        } catch (Exception e) {
            sb.append(" error ").append(e.getMessage());
        }
        return sb.toString();
    }

    private static void addLine(StringBuilder sb, String label, String text) {
        if (label != null && !label.isBlank()) sb.append(label);
        if (text != null) sb.append(text);
        sb.append("\n");
    }

}
