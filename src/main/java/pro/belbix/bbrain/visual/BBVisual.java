package pro.belbix.bbrain.visual;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.ColorRGBA;
import com.jme3.system.AppSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.models.Key;
import pro.belbix.bbrain.neuron.Neuron;
import pro.belbix.bbrain.processing.DecisionCenter;
import pro.belbix.bbrain.processing.InputProcessing;
import pro.belbix.bbrain.processing.ProcessingCenter;
import pro.belbix.bbrain.processing.XorProcessor;
import pro.belbix.bbrain.utils.Config;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by v.belykh on 12.07.2018
 */
public class BBVisual extends SimpleApplication implements Runnable {
    private static final Logger log = LogManager.getRootLogger();
    private static final ConcurrentHashMap<Key, Neuron> neurons = new ConcurrentHashMap<>();
    private BBVisual app;
    private BitmapText hudProcessingCenter;
    private BitmapText hudDecisionCenter;

    @Override
    public void run() {
        app = new BBVisual();
        app.setShowSettings(false);
        app.setPauseOnLostFocus(false);

        AppSettings s = new AppSettings(true);
        s.setResolution(Config.width, Config.height);
        s.setTitle("BBrain Prototype v.0.0.1");
        s.setSamples(0);
        s.setDepthBits(16);
        s.setAudioRenderer(null);
//        try {
//            s.setIcons(new BufferedImage[]{
//                    ImageIO.read(new File(this.getClass().getResource("icon16.png").toURI())),
//                    ImageIO.read(new File(this.getClass().getResource("icon32.png").toURI())),
//                    ImageIO.read(new File(this.getClass().getResource("icon128.png").toURI())),
//                    ImageIO.read(new File(this.getClass().getResource("icon256.png").toURI()))
//            });
//        } catch (Exception e) {
//            log.error("Error load icons", e);
//        }
        app.setSettings(s);

        app.start();
    }

    @Override
    public void simpleInitApp() {

        MainScene mainScene = new MainScene(assetManager);
        viewPort.attachScene(mainScene.getRootNode());
        stateManager.attach(mainScene);

        flyCam.setMoveSpeed(Config.moveSpeed);
        initKeys();
        attacheHUD();
    }

    @Override
    public void update() {
        super.update();

        float tpf = timer.getTimePerFrame();

        stateManager.update(tpf);
        stateManager.render(renderManager);

        renderManager.render(tpf, context.isRenderable());

        hudProcessingCenter.setText(ProcessingCenter.getState());
        hudDecisionCenter.setText(DecisionCenter.getInstance().getState());
    }

    private float rSpeed = 0;

    @Override
    public void loseFocus() {
        super.loseFocus();
        rSpeed = flyCam.getRotationSpeed();
        flyCam.setRotationSpeed(0);
    }

    @Override
    public void gainFocus() {
        super.gainFocus();
        if (rSpeed != 0) {
            flyCam.setRotationSpeed(rSpeed);
        }
    }

    private void initKeys() {
        inputManager.addMapping("F1", new KeyTrigger(KeyInput.KEY_F1));
        inputManager.addMapping("1", new KeyTrigger(KeyInput.KEY_1));
        inputManager.addMapping("2", new KeyTrigger(KeyInput.KEY_2));
        inputManager.addMapping("3", new KeyTrigger(KeyInput.KEY_3));
        inputManager.addMapping("4", new KeyTrigger(KeyInput.KEY_4));
        inputManager.addMapping("5", new KeyTrigger(KeyInput.KEY_5));
        inputManager.addListener(actionListener, "1", "2", "3", "4", "5");
        inputManager.addListener(actionListenerF, "F1");
    }

    private void attacheHUD(){
        float leftX = 0;
        hudProcessingCenter = createHUDText("hudProcessingCenter", leftX, settings.getHeight());
        guiNode.attachChild(hudProcessingCenter);
        hudDecisionCenter = createHUDText("hudDecisionCenter", leftX, settings.getHeight() - 200);
        guiNode.attachChild(hudDecisionCenter);
    }

    private BitmapText createHUDText(String text, float x, float y){
        BitmapText hudText = new BitmapText(guiFont, false);
        hudText.setSize(guiFont.getCharSet().getRenderedSize());      // font size
        hudText.setColor(ColorRGBA.Gray);                             // font color
        hudText.setText(text);             // the text
        hudText.setLocalTranslation(x, y, 0); // position
        return hudText;
    }

    private final ActionListener actionListener = (name, keyPressed, tpf) -> {
        if (!keyPressed) return;
        InputProcessing.getInstance().setNumericKeyToNeuron(name);
    };

    private final ActionListener actionListenerF = (name, keyPressed, tpf) -> {
        if (!keyPressed) return;
        switch (name) {
            case "F1":
                if (XorProcessor.pause)
                    XorProcessor.pause = false;
                else
                    XorProcessor.pause = true;
                break;
        }
    };

    public static ConcurrentHashMap<Key, Neuron> getNeurons() {
        return neurons;
    }

    public void close() {
        if (app != null) app.close();
    }

    public synchronized static boolean isNeuronInProcessing(Neuron neuron) {
        if (neurons.containsKey(neuron.getKey())) {
            return true;
        } else {
            neurons.put(neuron.getKey(), neuron);
            return false;
        }
    }
}
