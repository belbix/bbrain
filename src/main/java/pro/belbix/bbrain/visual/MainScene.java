package pro.belbix.bbrain.visual;

import com.jme3.app.state.AbstractAppState;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.models.Key;
import pro.belbix.bbrain.utils.Config;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by v.belykh on 25.07.2018
 */
public class MainScene extends AbstractAppState {
    private static final Logger log = LogManager.getRootLogger();
    private Node rootNode = new Node("NeuronUpdaterKafkaRootNode");
    private ConcurrentHashMap<Key, Node> nodes = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Spatial> globalSpatial = new ConcurrentHashMap<>();
    private ExecutorService executorService;
    private List<Callable<Integer>> tasks = new ArrayList<>();


    public MainScene(AssetManager assetManager) {
        executorService = new ThreadPoolExecutor(Config.updaterCount, Config.updaterCount, 1000L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());

        for (int i = 0; i < Config.updaterCount; i++) {
            tasks.add(new MessageReceiver(rootNode, nodes, globalSpatial, assetManager));
        }
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        LocalDateTime start = LocalDateTime.now();

        List<Future<Integer>> results = null;
        try {
            results = executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            log.error("", e);
        }
        int count = 0;
        int countThreads = 0;
        if (results != null) {
            for (Future<Integer> future : results) {
                try {
                    count += future.get();
                    countThreads++;
                } catch (InterruptedException | ExecutionException e) {
                    log.error("", e);
                }
            }
        }
        if(count > 0)
            log.debug(countThreads + " threads count:" + count + " by time "
                + Duration.between(start, LocalDateTime.now()));

        rootNode.updateLogicalState(tpf);
        rootNode.updateGeometricState();
    }

    public Node getRootNode() {
        return rootNode;
    }

    @Override
    public void cleanup() {
        super.cleanup();
        close();
        executorService.shutdown();
    }

    private void close() {

    }
}