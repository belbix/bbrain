package pro.belbix.bbrain.utils;

import pro.belbix.bbrain.models.Candle;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by v.belykh on 11.05.2018
 */
public class Utils {
    public static void sleep(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {}
    }

    public static int getRandom(int min, int max){
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public static double[] getMaxMin(double[] arr) {
        if (arr.length == 0) return new double[2];
        double max = arr[0];
        double min = arr[0];
        for (double d : arr) {
            if (max < d) {
                max = d;
            }
            if (min > d) {
                min = d;
            }
        }
        return new double[]{max, min};
    }

    public static String padLeft(String value, String c, int length) {
        if (value == null) value = "";
        int l = value.length();
        if (l >= length) {
            return value;
        }

        l = length - l;

        StringBuilder valueBuilder = new StringBuilder(value);
        for (int i = 1; i <= l; i++) {
            valueBuilder.insert(0, c);
        }

        return valueBuilder.toString();
    }

    public static double[] getClosesFromCandles(ArrayList<Candle> candles){
        double[] arr = null;
        arr = new double[candles.size()];
        int i = 0;
        for (Candle candle : candles) {
            arr[i] = candle.getClose();
            i++;
        }
        return arr;
    }

    public static LocalDateTime getCandlesMaxDate(ArrayList<Candle> candles){
        LocalDateTime maxDate = null;
        for (Candle candle : candles) {
            if(maxDate == null) maxDate = candle.getDate().toLocalDateTime();
            if(candle.getDate().toLocalDateTime().isAfter(maxDate)){
                maxDate = candle.getDate().toLocalDateTime();
            }
        }
        return maxDate;
    }

}
