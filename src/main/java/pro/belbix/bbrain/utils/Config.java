package pro.belbix.bbrain.utils;

/**
 * Created by v.belykh on 25.04.2018
 */
public class Config {
    //************************GRAPHIC********************************
    public static final boolean useLocalGraphic = true;
    public static final int updaterCount = 1;
    public static final int height = 850;
    public static final int width = 1800;
    public static final int moveSpeed = 100;

    //************************VISUAL********************************
    public static final int visualisationPause = 500;
    public static final float widthNeuron = 10;
    public static final float widthDendrite = widthNeuron * 0.7f;
    public static final float widthAxon = widthNeuron * 0.7f;
    public static final float heightLayer = 10;
    public static final float heightDendrite = heightLayer / 3;
    public static final float heightAxon = heightLayer / 6;
    public static final float heightReceiver = (heightLayer - heightDendrite) / 5;
    public static final boolean zLines = false;

    //************************DB********************************
    public static final String db_class = "com.mysql.cj.jdbc.Driver";
    //    public static String db_class = "org.h2.Driver";
    public static final String db_user = "belbix";
    public static final String db_password = "111111";
    public static final String db_url = "jdbc:mysql://localhost:3306/brain?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    //    public static String db_url = "jdbc:h2:tcp://localhost/mem:bb";
    public static final int db_initial_size = 1;
    public static final int db_max_idle = 10;
    public static final int db_min_idle = 1;
    public static final int db_max_total = -1;

    //************************DB STOCK ********************************
    public static final String stock_db_class = "com.mysql.cj.jdbc.Driver";
    public static final String stock_db_user = "belbix";
    public static final String stock_db_password = "111111";
    public static final String stock_db_url = "jdbc:mysql://localhost:3306/crypto?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    public static final int stock_db_initial_size = 1;
    public static final int stock_db_max_idle = 10;
    public static final int stock_db_min_idle = 1;
    public static final int stock_db_max_total = -1;

    //************************KAFKA ********************************
    public static final String KAFKA_BROKERS = "localhost:9092";
    public static final String CLIENT_ID = "client1";
    public static final String TOPIC_NAME_STATE = "bb_state";
    public static final String TOPIC_NAME_DELIVERY = "bb_delivery";
    public static final String GROUP_ID_CONFIG = "consumerGroup1";
    public static final String OFFSET_RESET_EARLIER = "earliest";
    public static final Integer MAX_PARTITION_FETCH_BYTES = 10485760;
    public static final Integer FETCH_MAX_BYTES = 524288000;
    public static final Integer MAX_POLL_RECORDS = 100000;
    public static final String partitionPattern = "part\\d"; //java pattern

    //**************************EYE*********************************
    public static final int eyeWidth = 3;
    public static final int eyeHeight = 3;
    public static final int eyeSleep = 1;
    public static final int photoreceptorTransmitterCount = 10;

    //************************CANDLE EYE*****************************
    public static final int neuronCount = 2;
    public static final int candleEyeBatch = neuronCount;
    public static final int candleEyeDays = 30;


    public static final long HUNDRED = 100;
    //***************************AXON*******************************
    public static final int axonDefaultBapResist = 0;
    public static final long axonDefaultTimeNeedToActivation = 0; //надо переделать мезанизм, и подумать нужен ли он вообще, сильно замедляет систему

    public static final long wishGetReceiverIncr = HUNDRED;
    public static final long wishGetReceiverMin = HUNDRED;
    public static final long createSenderBarrier = HUNDRED;
    public static final long wishCreateSenderIncr = createSenderBarrier / 10;

    //*************************RECEIVER*********************************
    public static final long wishDeathIncrBarrier = HUNDRED * 10;
    public static final long wishDeathMax = wishDeathIncrBarrier / 10;
    public static final float activityIncrement = HUNDRED;

    public static final long receiverReceptorDefaultEnergy = HUNDRED;
    public static final int maxReceivers = 3;
    public static final long createReceiverBarrier = HUNDRED;
    public static final long wishCreateReceiverIncr = createReceiverBarrier / 10;

    //**********************NEURON**************************************
    public static long epLifeTimeMax = HUNDRED * 20; //это абстракция времени жизни заряда на мембране

    public static double refractoryPeriodAbsolute = 0.6; // процент когда вообще не будет проводится сигнал
    public static long membraneResistanceDefault = HUNDRED * HUNDRED;
    public static long refractoryPeriodMax = membraneResistanceDefault * 2;

    public static final int maxDendrites = 3;
    public static final long wishDendriteBarrier = HUNDRED;
    public static final long wishDendriteIncr = wishDendriteBarrier / 10;

    //*****************TRANSMITTER*******************
    public static final int transmitterDefaultNeedActivation = 25; //not use
    public static final int transmitterDefaultCount = (int) HUNDRED;
    public static final double synapseRandom = 0.5;

    //************************ FINDING NEW LINK *****************************
    public static final int xDeep = 30;
    public static final int yDeep = 1;
    public static final int findingAttempts = 10;



}
