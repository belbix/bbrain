package pro.belbix.bbrain.processing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.build.NeuronFactory;
import pro.belbix.bbrain.neuron.*;
import pro.belbix.bbrain.utils.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static pro.belbix.bbrain.neuron.Layer.LayerType.RECEPTOR;

/**
 * Created by v.belykh on 9/24/2018
 */
public class NeuronHandler implements Runnable {
    private static final Logger log = LogManager.getRootLogger();
    private boolean run = true;

    @Override
    public void run() {
        main();
    }

    private void main() {
        while (run) {
            Neuron neuron = null;
            try {
                neuron = ProcessingCenter.getNeuronsQueue().poll(60, TimeUnit.SECONDS);
                if (neuron == null) continue;
                synchronized (neuron.lock) {
                    handle(neuron);
                }
            } catch (Exception e) {
                log.error("Error in NeuronHandler " + neuron, e);
            }
        }
    }

    private void handle(Neuron neuron) {
        if (neuron.getDendriteReceivers() == null) {
            log.error("Receiver is null in " + neuron);
            return;
        }
//        individualHandle(neuron);

        checkNeuronStructure(neuron);
        for (Dendrite dendrite : neuron.getDendrits()) {
            checkDendriteStructure(dendrite);
            for (Receiver receiver : dendrite.getReceivers()) {
                receiver.processing();
                checkReceiverStructure(receiver);
            }
        }

        boolean spikeCreated = neuron.processing();

        for (Axon axon : neuron.getAxons()) {
            checkAxonStructure(axon);
            ArrayList<Destination> destinations = axon.processing();
            if (destinations != null) {
                for (Destination destination : destinations) {
                    ProcessingCenter.sendToDestination(destination);
                }
            }
        }

        postProcessing(neuron, spikeCreated);

        if (neuron.getLastVisualization() == null)
            ProcessingCenter.addNeuronForVisual(neuron);
        log.trace("handle " + neuron);

    }

    private void postProcessing(Neuron neuron, boolean sent) {
        switch (neuron.getLayer().getLayerType()) {
            case MANIPULATOR:
                DecisionCenter.getInstance().postProcessing(neuron, sent);
                break;
        }
    }

    private void checkNeuronStructure(Neuron neuron) {
        if (neuron.getLayer().getLayerType().equals(RECEPTOR)) return;
        if (neuron.getWishDendrite() > Config.wishDendriteBarrier) {
            Dendrite dendrite = new Dendrite(neuron);
            dendrite.setId(neuron.nextDendriteId());
            dendrite.setWishReceiver(Config.createReceiverBarrier + 1);
            neuron.setWishDendrite(0);
        } else if (neuron.getWishDendrite() < -Config.wishDendriteBarrier) {
            neuron.setWishDendrite(0);
            if (neuron.getDendrits().size() <= 1) {
                return;
            }
            List<Receiver> receivers = neuron.deleteDendrite();
            for (Receiver receiver : receivers) {
                ProcessingCenter.deleteReceiver(receiver);
            }
            neuron.setWishDendrite(0);
        }
    }

    private void checkReceiverStructure(Receiver receiver) {
        if (receiver.getWishSender() < -Config.createReceiverBarrier) {
            receiver.deleteSender();
            receiver.setWishSender(0);
        }

        if(receiver.getDeath() > receiver.getDendrite().getNeuron().getLayer().getDeathMax()){
            receiver.delete();
        }
    }

    private void checkDendriteStructure(Dendrite dendrite) {
        if (dendrite.getWishReceiver() > Config.createReceiverBarrier) {
            Receiver receiver = NeuronFactory.addReceiver(dendrite, NeuronFactory.randomSynapse());
            ProcessingCenter.registerReceiver(receiver);
            dendrite.setWishReceiver(0);
        } else if (dendrite.getWishReceiver() < -Config.createReceiverBarrier) {
            dendrite.setWishReceiver(0);
            if (dendrite.getReceivers().size() <= 1) {
                return;
            }
            Receiver receiver = dendrite.deleteReceiver();
            ProcessingCenter.deleteReceiver(receiver);
            dendrite.setWishReceiver(0);
        }

    }

    private void checkAxonStructure(Axon axon) {
        if (axon.getWishCreateSender() > Config.createSenderBarrier) {
            NeuronFactory.addSender(axon, NeuronFactory.randomSynapse(axon.getNeuron().getLayer().getAxonSynapses()));
            axon.setWishCreateSender(0);
        }

        for (Sender sender : axon.getSenders()) {
            if (sender.getReceiver() == null) {
                if (sender.getWishGetReceiver() < Config.wishGetReceiverMin) continue;
                Receiver receiver = ProcessingCenter.bestReceiverAround(sender);
                if (receiver != null) {
                    sender.setReceiver(receiver);
                    sender.setWishGetReceiver(0);
                }
            }
        }
    }

}
