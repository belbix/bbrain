package pro.belbix.bbrain.processing;

import com.jme3.math.Vector3f;
import pro.belbix.bbrain.build.NeuronFactory;
import pro.belbix.bbrain.neuron.*;
import pro.belbix.bbrain.utils.Config;

import java.util.*;

import static pro.belbix.bbrain.neuron.Layer.LayerType.*;

public class CenterUtils {

    public static Receiver findReceiverForConnection(Map<Long, Layer> layers, Layer.LayerType layerType, Synapse.Type synapseType, HashMap<String, Neuron> connectedNeurons) {
        Layer associativeLayer = layers.values().stream().filter(x -> x.getLayerType().equals(layerType)).findAny().orElse(null);
        if (associativeLayer == null) return null;
        for (Neuron neuron : associativeLayer.getNeurons().values()) {
            if(connectedNeurons.containsKey(neuron.getName())) continue;
            for (Dendrite dendrite : neuron.getDendrits()) {
                for (Receiver receiver : dendrite.getReceivers()) {
                    if (receiver.getSender() != null) continue;
                    Receptor receptor = receiver.getReceptors().stream().filter(x -> x.getSynapse().getType().equals(synapseType)).findAny().orElse(null);
                    if (receptor == null) {
                        NeuronFactory.createReceptor(receiver.getReceptors().size(), receiver, new Synapse(synapseType), 0);
                    }
                    return receiver;
                }
            }
            Dendrite dendrite = neuron.getDendrits().stream().findAny().orElse(null);
            if (dendrite == null) continue;
            return ProcessingCenter.createReceiver(dendrite, synapseType);
        }
        return null;
    }

    public static Receiver findReceiver(Map<Long, Layer> layers, Layer.LayerType layerType, boolean create) {
        Neuron neuron = findNeuron(layers, layerType);
        if (neuron == null) return null;
        Dendrite dendrite = neuron.getDendrits().stream().findAny().orElse(null);
        if (dendrite == null) return null;
        Receiver receiver = dendrite.getReceivers().stream().filter(x -> x.getSender() == null).findAny().orElse(null);
        if (receiver != null) return receiver;
        if (create) {
            receiver = NeuronFactory.createReceiver(dendrite.nextReceiverId(), dendrite);
        }
        return receiver;
    }

    public static Neuron findNeuron(Map<Long, Layer> layers, Layer.LayerType layerType) {
        Layer layer = layers.values().stream().filter(x -> x.getLayerType().equals(layerType)).findAny().orElse(null);
        if (layer == null) return null;
        return layer.getNeurons().values().stream().findAny().orElse(null);
    }

    public static boolean simpleActivation(Neuron neuron, boolean clear) {
        ArrayList<Receiver> receivers = neuron.getDendriteReceivers();
        boolean activate = false;
        for (Receiver receiver : receivers) {
            if (receiver.getTransmitters().isEmpty()) continue;
            activate = true;
            if (clear) receiver.getTransmitters().clear();
        }
        return activate;
    }

    public static Receiver findCompareReceiver(Neuron neuron, Set<Synapse.Type> transmitterTypes, boolean free) {
        for (Receiver receiver : neuron.getDendriteReceivers()) {
            for (Receptor receptor : receiver.getReceptors()) {
                if (transmitterTypes.contains(receptor.getSynapse().getType())) {
                    if (free) {
                        if (receiver.getSender() == null) {
                            return receiver;
                        }
                    } else {
                        return receiver;
                    }
                }
            }
        }
        return null;
    }

    public static Dendrite findBestDendrite(Neuron neuron) {
        double bestActivity = -1;
        Dendrite bestDendrite = null;
        for (Dendrite dendrite : neuron.getDendrits()) {
            if (dendrite.getActivity() > bestActivity) {
                bestDendrite = dendrite;
            }
        }
        return bestDendrite;
    }

    private static boolean checkReceptor(Receiver currentReceiver, Set<Synapse.Type> transmitterTypes) {
        for (Receptor receptor : currentReceiver.getReceptors()) {
            if (transmitterTypes.contains(receptor.getSynapse().getType())) {
                return true;
            }
        }
        return false;
    }

    private static Receiver findBestReceiver(Neuron currentNeuron, Neuron senderNeuron, double bestActivity, double power, Set<Synapse.Type> transmitterTypes) {
        Receiver bestReceiver = null;
        for (Dendrite currentDendrite : currentNeuron.getDendrits()) {
            double activity = calcActivity(senderNeuron, currentNeuron, currentDendrite.getActivity(), power);
            for (Receiver currentReceiver : currentDendrite.getReceivers()) {
                if (currentReceiver == null) continue;


                if (activity > bestActivity) {
                    bestActivity = activity;
//                    bestDendrite = currentDendrite;
                    if (currentReceiver.getSender() != null) continue; //только на не занятые приемники)
//                                if (!checkReceptor(currentReceiver, transmitterTypes))
//                                    continue; //только имеющие нужный рецептор
                    NeuronFactory.createReceptorWithAnySynapse(currentReceiver, transmitterTypes);
                    bestReceiver = currentReceiver;
                }
            }
        }
        return bestReceiver;
    }

    private static double calcActivity(Neuron senderNeuron, Neuron currentNeuron, double currentActivity, double power) {
        double activity = currentActivity;
        if (activityRate(senderNeuron.getLayer(), currentNeuron.getLayer())) activity += Config.activityIncrement;
        activity += power;
        Vector3f senderV = new Vector3f(senderNeuron.getX(), senderNeuron.getY(), senderNeuron.getY());
        Vector3f currentV = new Vector3f(currentNeuron.getX(), currentNeuron.getY() - 1, currentNeuron.getZ());
        float distance = senderV.distance(currentV) / 2;
        double distanceDecrement = Config.activityIncrement - (Config.activityIncrement / distance);
        if (distanceDecrement > 0) activity -= distanceDecrement;
        return activity;
    }

    private static boolean activityRate(Layer connectLayer, Layer forConnect) {
        if (true) return false;
        switch (connectLayer.getLayerType()) {
            case RECEPTOR:
                if (ASSOCIATIVE.equals(forConnect.getLayerType()))
                    return true;
            case ASSOCIATIVE:
                if (PROCESSING.equals(forConnect.getLayerType()))
                    return true;
                if (MANIPULATOR.equals(forConnect.getLayerType()))
                    return true;
            case PROCESSING:
                if (MEMORY.equals(forConnect.getLayerType()))
                    return true;
            case MEMORY:
                if (MANIPULATOR.equals(forConnect.getLayerType()))
                    return true;
        }
        return false;
    }

    public static boolean checkAlreadyConnected(Neuron senderNeuron, Neuron currentNeuron) {
        if(senderNeuron.getName().equals(currentNeuron.getName())) return true;
        for (Axon axon : senderNeuron.getAxons()) {
            for (Sender sender : axon.getSenders()) {
                if (sender.getReceiver() == null) continue;
                long snId = sender.getReceiver().getDendrite().getNeuron().getId();
                long slId = sender.getReceiver().getDendrite().getNeuron().getLayerId();
                if (slId == currentNeuron.getLayerId()
                        && snId == currentNeuron.getId()
                ) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean xor(double[] inputValues){
        if(inputValues == null || inputValues.length != 2)
            throw new IllegalStateException("Wrong structure for " + Arrays.toString(inputValues));
        if(inputValues[0] == 1 && inputValues[1] == 0) return true;
        if(inputValues[0] == 0 && inputValues[1] == 1) return true;
        if(inputValues[0] == 1 && inputValues[1] == 1) return false;
        if(inputValues[0] == 0 && inputValues[1] == 0) return false;
        throw new IllegalStateException("Wrong structure for " + Arrays.toString(inputValues));
    }

}
