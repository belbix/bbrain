package pro.belbix.bbrain.processing;

import pro.belbix.bbrain.neuron.Layer;
import pro.belbix.bbrain.neuron.Neuron;

import java.time.Instant;

public class NeuronRefresher implements Runnable {

    private static final int PAUSE = 500;

    @Override
    public void run() {
        while (true) {
            process();
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) { }
        }
    }

    private void process() {
        Instant now = Instant.now();
        for (Layer layer : ProcessingCenter.getLayers().values()) {
            for (Neuron neuron : layer.getNeurons().values()) {
//                if (neuron.getLastVisualization() == null) continue;
//                if (!Duration.between(neuron.getLastVisualization(), now).minus(Duration.of(PAUSE, ChronoUnit.MILLIS)).isNegative())
//                    ProcessingCenter.getNeuronsQueue().offer(neuron);
                    ProcessingCenter.addNeuronForVisual(neuron);
            }
        }
    }
}
