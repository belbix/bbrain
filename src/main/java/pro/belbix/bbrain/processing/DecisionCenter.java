package pro.belbix.bbrain.processing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.neuron.Layer;
import pro.belbix.bbrain.neuron.Neuron;
import pro.belbix.bbrain.neuron.Synapse;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class DecisionCenter {

    private static final Logger log = LogManager.getRootLogger();
    private double[] inputValues;
    private String state = null;
    private Instant lastState = null;
    private String lastInfo;

    private final static DecisionCenter ourInstance = new DecisionCenter();

    public static DecisionCenter getInstance() {
        return ourInstance;
    }

    private DecisionCenter() {
    }

    public synchronized void postProcessing(Neuron neuron, boolean sent) {
        boolean decision = CenterUtils.xor(inputValues);
        boolean good;
        if (decision) {
            good = sent;
        } else {
            good = !sent;
        }
        lastInfo = Arrays.toString(inputValues) + " XOR: " + decision + " sent: " + sent + " " + (good ? "GOOD" : "BAD");
        neuron.getInputInfo().setValueInstantly(lastInfo);

//        if(sent) return;

        decision(good);
    }

    public void decision(boolean good) {
        Neuron n;
        if (good) {
            n = CenterUtils.findNeuron(ProcessingCenter.getLayers(), Layer.LayerType.PLEASURE);
        } else {
            n = CenterUtils.findNeuron(ProcessingCenter.getLayers(), Layer.LayerType.PUNISHMENT);
        }
        if (n != null) {
            n.createSpikeInAxon();
            ProcessingCenter.getNeuronsQueue().add(n);
        }
    }

    public void setInputValues(double[] inputValues) {
        this.inputValues = inputValues;
    }

    public String getState() {
        if (lastState == null || Duration.between(lastState, Instant.now()).toMillis() > 100) {
            String s = "|DecisionState|" + "\n";
            s += "lastInfo: " + lastInfo + "\n";
            s += "xorPause: " + XorProcessor.pause + "\n";
            state = s;
            lastState = Instant.now();
        }
        return state;
    }
}
