package pro.belbix.bbrain.processing;

import com.jme3.math.Vector3f;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.build.LayerCreator;
import pro.belbix.bbrain.build.MindBox;
import pro.belbix.bbrain.build.NeuronFactory;
import pro.belbix.bbrain.models.Key;
import pro.belbix.bbrain.models.SimpleKey;
import pro.belbix.bbrain.neuron.*;
import pro.belbix.bbrain.utils.Config;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import static pro.belbix.bbrain.neuron.Layer.LayerType.*;

/**
 * Created by v.belykh on 9/25/2018
 */
public class ProcessingCenter {
    private static final Logger log = LogManager.getRootLogger();
    private static Map<Long, Layer> layers = new ConcurrentHashMap<>();
    private static Map<Key, Receiver> receivers = new ConcurrentHashMap<>();
    private static BlockingQueue<Neuron> neuronsQueue = new LinkedBlockingQueue<>();

    private static BlockingQueue<Neuron> neuronForVisual = new LinkedBlockingQueue<>();
    private static Map<Long, Neuron> receptors = null;
    private static Map<Long, Neuron> manipulators = null;
    private static float maxZ = 0;
    private static float maxX = 0;
    private static String state = null;
    private static Instant lastState = null;
    private static final HashSet<Layer.LayerType> targetsFroSpecialLayerTypes = new HashSet<>(Arrays.asList(ASSOCIATIVE, MANIPULATOR));


    public static void initLayers() {
        LayerCreator.getInstance().init();
        ArrayList<Layer> layers = LayerCreator.getInstance().createLayers();
        MindBox mindBox = new MindBox();
        for (Layer layer : layers) {
            ProcessingCenter.layers.put(layer.getId(), layer);
            switch (layer.getLayerType()) {
                case RECEPTOR:
                    NeuronFactory.createNeuronFromLayer(layer);
                    receptors = layer.getNeurons();
                    break;
                case ASSOCIATIVE:
                case PROCESSING:
                case MEMORY:
                case PLEASURE:
                case PUNISHMENT:
                    NeuronFactory.createNeuronFromLayer(layer);
                    break;
                case MANIPULATOR:
                    NeuronFactory.createNeuronFromLayer(layer);
                    manipulators = layer.getNeurons();
                    break;
            }

            for (Neuron neuron : layer.getNeurons().values()) {
                for (Receiver receiver : neuron.getDendriteReceivers()) {
                    registerReceiver(receiver);
                }
                NeuronFactory.calcCoordinates(neuron, mindBox);
            }
        }

        //for more clear coordinates use twice
        mindBox.clearCoordinates();
        for (Layer layer : layers) {
            for (Neuron neuron : layer.getNeurons().values()) {
                NeuronFactory.calcCoordinates(neuron, mindBox);
                if (neuron.getZ() > maxZ) maxZ = neuron.getZ();
            }
            if (layer.getNeurons().size() > maxX) maxX = layer.getNeurons().size();
        }

        log.info("initLayers end");
    }

    public static void registerReceiver(Receiver receiver) {
        SimpleKey key = new SimpleKey();
        key.setLong1(receiver.getLayer_id());
        key.setLong2(receiver.getNeuron_id());
        key.setLong3(receiver.getDendrite_id());
        key.setLong4(receiver.getId());
        receivers.put(key, receiver);
    }

    public static void sendToDestination(Destination destination) {
        SimpleKey key = new SimpleKey();
        key.setLong1(destination.getLayerId());
        key.setLong2(destination.getNeuronId());
        key.setLong3(destination.getDendriteId());
        key.setLong4(destination.getReceiverId());
        Receiver receiver = receivers.get(key);
        if (receiver != null) {
            newLocalDelivery(receiver, destination.getTransmitters());
        } else {
            //TODO send in external server
            log.error("receiver not found local " + destination);
        }
    }

    private static void newLocalDelivery(Receiver receiver, List<Transmitter> transmitters) {
        if (receiver == null) {
            log.error("Receiver is null");
            return;
        }
        HashSet<Neuron> neurons = new HashSet<>(); //чтобы не дублировать нейроны для обработки
        for (Transmitter transmitter : transmitters) {
            receiver.addTransmitter(transmitter);
            if (receiver.getDendrite() == null) {
                log.error("receiver.getDendrite() is null for " + receiver);
                return;
            }
            if (receiver.getDendrite().getNeuron() == null) {
                log.error("receiver.getDendrite().getNeuron() is null for " + receiver);
                return;
            }
            neurons.add(receiver.getDendrite().getNeuron());
        }
        ProcessingCenter.getNeuronsQueue().addAll(neurons);
        log.trace("newLocalDelivery in" + neurons + " " + transmitters);
    }

    public synchronized static Receiver bestReceiverAround(Sender senderIn) {
        Neuron bestNeuron = findBestNeuronForConnect(layers,
                senderIn.getAxon().getNeuron(),
                senderIn.getAxon().getNeuron().getLayer().getLayerType());
        if (bestNeuron == null) {
            return null;
        }

        Receiver bestReceiver = CenterUtils.findCompareReceiver(bestNeuron, senderIn.transmittersTypes(), true);

        if (bestReceiver != null) return bestReceiver;

        Dendrite bestDendrite = CenterUtils.findBestDendrite(bestNeuron);
        bestReceiver = createReceiver(bestDendrite, senderIn.transmittersTypes().stream().findAny().orElse(null));

        return bestReceiver;
    }

    private static Neuron findBestNeuronForConnect(Map<Long, Layer> layers, Neuron senderNeuron, Layer.LayerType senderLayerType) {
        Vector3f senderV = new Vector3f(senderNeuron.getX(), senderNeuron.getY(), senderNeuron.getZ());
        float maxDistance = new Vector3f(0, 0, 0).distance(
                new Vector3f(ProcessingCenter.getMaxX(),
                        layers.size(),
                        ProcessingCenter.getMaxZ()));

        TreeMap<Double, Neuron> neurons = new TreeMap<>();
        for (Layer layer : layers.values()) {

            if(skipSpecialLayerType(senderLayerType, layer)) continue;

            for (Neuron neuron : layer.getNeurons().values()) {
                if (senderNeuron.getName().equals(neuron.getName())
                        || neuron.getDendrits().isEmpty()
                        || CenterUtils.checkAlreadyConnected(senderNeuron, neuron)
                ) continue;

                Vector3f vector = new Vector3f(neuron.getX(), neuron.getY(), neuron.getZ());
                float distance = senderV.distance(vector);
                double kDistance = distance / maxDistance;
                if (kDistance > 1) kDistance = 1;
                if (neuron.getY() <= senderNeuron.getY()) kDistance *= 2;

                double activity = neuron.getActivity();
                double kActivity = (activity / layer.getActivityMax());
                if (kActivity > 1) kActivity = 1;

                double d = kActivity - kDistance;
                neurons.put(d, neuron);
            }
        }
        if (neurons.isEmpty()) return null;
        return neurons.lastEntry().getValue();
    }

    private static boolean skipSpecialLayerType(Layer.LayerType senderLayerType, Layer layer){
        if(senderLayerType == null) return false;

        switch (senderLayerType){
            case PUNISHMENT:
            case PLEASURE:
                return !targetsFroSpecialLayerTypes.contains(layer.getLayerType());
        }
        return false;
    }

    private static long calculateStartNeuronId(Layer currentLayer, Layer senderLayer, Neuron senderNeuron) {
        if (currentLayer.getNeurons().size() < senderLayer.getNeurons().size()) {
            double kNeuron = (double) currentLayer.getNeurons().size() / (double) senderLayer.getNeurons().size();
            return Math.round(kNeuron * (double) (senderNeuron.getId()));
        }
        return 0;
    }

    public static Receiver createReceiver(Dendrite dendrite, Synapse.Type type) {
        if (dendrite == null || type == null) return null;
        Receiver receiver = NeuronFactory.addReceiver(dendrite, new Synapse(type));
        ProcessingCenter.registerReceiver(receiver);
        return receiver;
    }


    public static void addNeuronForVisual(Neuron neuron) {
        if (Config.useLocalGraphic) {
            if (!ProcessingCenter.getNeuronForVisual().offer(neuron)) {
                log.info("NeuronForVisual queue is full");
            }
        }
    }

    public static void deleteReceiver(Receiver receiver) {
        if (receiver == null) return;
        SimpleKey key = new SimpleKey();
        key.setLong1(receiver.getLayer_id());
        key.setLong2(receiver.getNeuron_id());
        key.setLong3(receiver.getDendrite_id());
        key.setLong4(receiver.getId());
        receivers.remove(key);
    }

    public static String getState(){
        if(lastState == null || Duration.between(lastState, Instant.now()).toMillis() > 100) {
            String s = "|PCState|"  + "\n";
            s += "layers: " + layers.size() + "\n";
            s += "receivers: " + receivers.size() + "\n";
            s += "neuronsQueue: " + neuronsQueue.size() + "\n";
            s += "neuronForVisual: " + neuronForVisual.size() + "\n";
            s += "receptors: " + receptors.size() + "\n";
            s += "manipulators: " + manipulators.size() + "\n";
            s += "maxZ: " + maxZ + "\n";
            s += "maxX: " + maxX + "\n";
            state = s;
            lastState = Instant.now();
        }
        return state;
    }

    public static Map<Long, Layer> getLayers() {
        return layers;
    }

    public static Map<Long, Neuron> getReceptors() {
        return receptors;
    }

    public static Map<Long, Neuron> getManipulators() {
        return manipulators;
    }

    public static BlockingQueue<Neuron> getNeuronsQueue() {
        return neuronsQueue;
    }

    public static BlockingQueue<Neuron> getNeuronForVisual() {
        return neuronForVisual;
    }

    public static float getMaxZ() {
        return maxZ;
    }

    public static float getMaxX() {
        return maxX;
    }
}
