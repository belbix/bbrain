package pro.belbix.bbrain.processing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.build.NeuronFactory;
import pro.belbix.bbrain.neuron.Neuron;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;

public class XorProcessor implements Runnable{
    private static final Logger log = LogManager.getRootLogger();
    private Map<Long, Neuron> receptorsNeurons = null;
    private Map<Long, Neuron> manipulators = null;
    private boolean run = true;
    private Neuron neuron = null;
    private long epCount = 0;
    private static final int PAUSE = 1000;
    public static boolean pause = false;


    @Override
    public void run() {
        neuron = NeuronFactory.createEmptyNeuron(0);
        try {
            while (run) {
                try {
                    Instant start = Instant.now();
                    double[] values = generateValues(2);
                    setValueToReceptors(values);
                    DecisionCenter.getInstance().setInputValues(values);
                    long ms = Duration.between(start, Instant.now()).toMillis();
                    long delay = PAUSE - ms;
                    if (delay > 0) {
                        Thread.sleep(delay);
                    }

                    while (pause){
                        Thread.sleep(100);
                    }

                } catch (Exception e) {
                    log.error("Error processing", e);
                    Thread.sleep(1);
                }
            }
        } catch (Exception e) {
            log.error(XorProcessor.class.getSimpleName() + " error", e);
        }
    }

    private double[] generateValues(int count){
        double[] values = new double[count];
        for(int i = 0; i < count; i++){
            double r = Math.random();
            if(r < 0.5) values[i] = 0;
            else values[i] = 1;
        }
        return values;
    }

    private void setValueToReceptors(double[] values) {
        if (receptorsNeurons == null || receptorsNeurons.isEmpty()) return;
        if (values == null) return;
        int arrLength = values.length;

        int count = 0;
        for (Neuron neuron : receptorsNeurons.values()) {
            synchronized (neuron.lock) {
                if (count > arrLength) {
                    log.warn("not enough closes count:" + count + " arrLength:" + arrLength);
                    break;
                }
                double value = values[count];
                if (value == 1) {
                    neuron.getPotentials().add(NeuronFactory.createDefaultEP(value, neuron, nextEpCount()));

                }
                neuron.getInputInfo().setValueInstantly(value + "");
                if (!ProcessingCenter.getNeuronsQueue().offer(neuron)) {
                    log.warn("NeuronQueue is full");
                }
            }
            count++;
        }
    }



    private long nextEpCount() {
        return epCount++;
    }

    public void setReceptorsNeurons(Map<Long, Neuron> receptorsNeurons) {
        this.receptorsNeurons = receptorsNeurons;
    }

    public void setManipulators(Map<Long, Neuron> manipulators) {
        this.manipulators = manipulators;
    }
}
