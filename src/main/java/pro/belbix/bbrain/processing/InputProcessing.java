package pro.belbix.bbrain.processing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.build.NeuronFactory;
import pro.belbix.bbrain.neuron.Neuron;

import java.util.Map;

public class InputProcessing {
    private static final Logger log = LogManager.getRootLogger();
    private static InputProcessing ourInstance = new InputProcessing();
    public static InputProcessing getInstance() {
        return ourInstance;
    }

    private Neuron neuron = null;
    private Map<Long, Neuron> receptorsNeurons = null;
    private long epCount = 0;

    private InputProcessing() {
        this.neuron = NeuronFactory.createEmptyNeuron(0);
        this.receptorsNeurons = ProcessingCenter.getReceptors();
    }

    public void setNumericKeyToNeuron(String key){
        if (receptorsNeurons == null || receptorsNeurons.isEmpty() || key == null) return;
        Long id = Long.parseLong(key) - 1;
        Neuron neuron = receptorsNeurons.get(id);
        if(neuron == null) return;
        neuron.getPotentials().add(NeuronFactory.createDefaultEP(1, this.neuron, nextEpCount()));
        neuron.getInputInfo().setValueInstantly("key " + key);
        if (!ProcessingCenter.getNeuronsQueue().offer(neuron)) {
            log.warn("NeuronQueue is full");
        }
    }

    private long nextEpCount() {
        return epCount++;
    }
}
