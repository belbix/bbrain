/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pro.belbix.bbrain.db;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.utils.Config;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {
    private static final Logger log = LogManager.getRootLogger();
    private static BasicDataSource ds;

    private static class SingletonHolder {
        private static final ConnectionPool HOLDER_INSTANCE = new ConnectionPool();
    }

    public static ConnectionPool getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    private ConnectionPool() {
        openPool();
    }

    private void openPool() {
        ds = new BasicDataSource();
        ds.setDriverClassName(Config.db_class);
        ds.setUsername(Config.db_user);
        ds.setPassword(Config.db_password);
        ds.setUrl(Config.db_url);
        ds.setInitialSize(Config.db_initial_size);
        ds.setMaxIdle(Config.db_max_idle);
        ds.setMaxTotal(Config.db_max_total);
        ds.setMinIdle(Config.db_min_idle);

        ds.setPoolPreparedStatements(true);
        ds.setMaxOpenPreparedStatements(500);
        ds.setDefaultAutoCommit(true);

        ds.setTimeBetweenEvictionRunsMillis(5000);
        ds.setRemoveAbandonedOnMaintenance(true);
        ds.setRemoveAbandonedTimeout(2000);

        ds.setValidationQuery("SELECT 1");
        ds.setValidationQueryTimeout(5);

        ds.setTestWhileIdle(true);
        log.debug("ConnectionPool activated.");
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public void putConnection(Connection connection) throws SQLException {
        connection.close();
    }

    public void closePool(){
        try {
            ds.close();
        } catch (SQLException e) {
            log.error("Error close pool", e);
        }
    }
}
