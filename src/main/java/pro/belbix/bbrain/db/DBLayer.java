package pro.belbix.bbrain.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.models.DBModel;
import pro.belbix.bbrain.models.Id;
import pro.belbix.bbrain.models.Table;
import pro.belbix.bbrain.models.Transient;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by v.belykh on 25.05.2018
 */
public class DBLayer {
    private static final Logger log = LogManager.getRootLogger();
    Connection conn = null;

    public void openConnection() throws SQLException {
        if (conn == null || conn.isClosed()) {
            conn = ConnectionPool.getInstance().getConnection();
        } else {
            log.debug("Connection already opened!");
        }
    }

    public void close() {
        try {
            ConnectionPool.getInstance().putConnection(conn);
        } catch (SQLException e) {
            log.error("Close db error", e);
        }
    }

    public void closePool() {
        ConnectionPool.getInstance().closePool();
    }

    public void checkConnection() throws SQLException {
        if (conn == null || conn.isClosed()) {
            openConnection();
        }
    }

    public Connection getConn() {
        return conn;
    }

    public ResultSet executeStatement(PreparedStatement ps) throws SQLException {
//        log.trace("sql:" + ps.toString());
        return ps.executeQuery();
    }

    public int executeUpdate(PreparedStatement ps) throws SQLException {
        log.trace("sql:" + ps.toString());
        return ps.executeUpdate();
    }


    public static DBModel createModelFromRs(ResultSet rs, DBModel dbModel) throws SQLException {
        Method[] methods = dbModel.getClass().getDeclaredMethods();
        String tableName = dbModel.getClass().getAnnotation(Table.class).name();
        Set<String> transients = new HashSet<>();

        try {
            for (Field field : dbModel.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(Transient.class)){
                    transients.add(field.getName());
                }
            }
        } catch (Exception e) {
            log.warn("Error set ids in class:" + dbModel.getClass().getName());
        }


        for (Method method : methods) {
            String mname = method.getName();
            if (!mname.startsWith("set")) {
                continue;
            }
            String columnName = mname.split("set")[1].toLowerCase();

            if (transients.contains(columnName)) continue;

            Type[] pType = method.getGenericParameterTypes();
            if (pType.length != 1) {
                continue;
            }

            Object o = rsMapper(pType[0].getTypeName(), rs, tableName + "." + columnName);

            if (o == null) continue;

            try {
                method.setAccessible(true);
                method.invoke(dbModel, o);

            } catch (InvocationTargetException | IllegalAccessException e) {
                log.error("", e);
            }
        }
        return dbModel;
    }

    public static void valueMapper(int i, Object value, PreparedStatement ps) throws SQLException, ClassCastException {

        switch (value.getClass().getName()) {
            case "java.lang.String":
                if (value instanceof String) ps.setString(i, (String) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "boolean":
            case "java.lang.Boolean":
                if (value instanceof Boolean) ps.setBoolean(i, (Boolean) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "java.lang.Byte": //what doing with byte[]?
            case "byte": //what doing with byte[]?
                if (value instanceof Byte) ps.setByte(i, (Byte) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "java.lang.Short":
            case "short":
                if (value instanceof Short) ps.setShort(i, (Short) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "int":
            case "java.lang.Integer":
                if (value instanceof Integer) ps.setInt(i, (Integer) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "long":
            case "java.lang.Long":
                if (value instanceof Long) ps.setLong(i, (Long) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "float":
            case "java.lang.Float":
                if (value instanceof Float) ps.setFloat(i, (Float) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "double":
            case "java.lang.Double":
                if (value instanceof Double) ps.setDouble(i, (Double) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "java.sql.Date":
                if (value instanceof Date) ps.setDate(i, (Date) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "java.sql.Time":
                if (value instanceof Time) ps.setTime(i, (Time) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "java.sql.Timestamp":
                if (value instanceof Timestamp) ps.setTimestamp(i, (Timestamp) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            case "java.io.InputStream":
                if (value instanceof InputStream) ps.setAsciiStream(i, (InputStream) value);
                else throw new ClassCastException("Value is not this class!");
                break;
            default:
                throw new RuntimeException("Unsupported class! " + value.getClass().getName());
        }

    }

    public static Object rsMapper(String typeName, ResultSet rs, String name) throws SQLException {
        switch (typeName) {
            case "java.lang.String":
                return rs.getString(name);
            case "java.lang.Boolean":
                boolean b = rs.getBoolean(name);
                return rs.wasNull() ? null : b;
            case "boolean":
                return rs.getBoolean(name);
            case "java.lang.Byte":
                byte bt = rs.getByte(name);
                return rs.wasNull() ? null : bt;
            case "byte":
                return rs.getByte(name);
            case "java.lang.Short":
                short sh = rs.getShort(name);
                return rs.wasNull() ? null : sh;
            case "short":
                return rs.getShort(name);
            case "java.lang.Integer":
                int i = rs.getInt(name);
                return rs.wasNull() ? null : i;
            case "int":
                return rs.getInt(name);
            case "long":
                return rs.getLong(name);
            case "java.lang.Long":
                long l = rs.getLong(name);
                return rs.wasNull() ? null : l;
            case "java.lang.Float":
                float f = rs.getFloat(name);
                return rs.wasNull() ? null : f;
            case "float":
                return rs.getFloat(name);
            case "java.lang.Double":
                double d = rs.getDouble(name);
                return rs.wasNull() ? null : d;
            case "double":
                return rs.getDouble(name);
            case "java.sql.Date":
                return rs.getDate(name);
            case "java.sql.Time":
                return rs.getTime(name);
            case "java.sql.Timestamp":
                return rs.getTimestamp(name);
            case "java.io.InputStream":
                return rs.getAsciiStream(name);
            default:
                throw new RuntimeException("Unsupported class! " + typeName);
        }
    }

    public int mergeModel(DBModel dbModel) throws SQLException {
        Set<String> ids = new HashSet<>();
        String tableName = "";

        try {
            String tableNameTmp = dbModel.getClass().getAnnotation(Table.class).name();
            if (!tableNameTmp.equals("")) tableName = tableNameTmp;
        } catch (Exception e) {
            log.debug("Table not set in class:" + dbModel.getClass().getName());
        }

        try {
            for (Field field : dbModel.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(Id.class)) {
                    ids.add(field.getName());
                }
            }
        } catch (Exception e) {
            log.warn("Error set ids in class:" + dbModel.getClass().getName());
        }

        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ").append(tableName).append("(");
        Map<String, Object> values = new LinkedHashMap<>();
        Method[] methods = dbModel.getClass().getDeclaredMethods();
        for (Method method : methods) {
            String mname = method.getName();
            if (!mname.startsWith("get") && !mname.startsWith("is")) {
                continue;
            }
            String columnName = null;
            if (mname.startsWith("get")) {
                columnName = mname.replaceFirst("get", "").toLowerCase();
            } else if (mname.startsWith("is")) {
                columnName = mname.replaceFirst("is", "").toLowerCase();
            }

            Type pType = method.getReturnType();
            if (pType == null) {
                continue;
            }
            try {
                method.setAccessible(true);
                Object result = method.invoke(dbModel);
                if (result != null) {
                    sb.append(columnName).append(",");
                    values.put(columnName, result);
                }

            } catch (Exception e) {
                String desc = "InternalError - Error create model. Error invoke:" + columnName;
                log.error(desc, e);
            }

        }

        if (values.isEmpty()) return -1;

        sb.setLength(sb.length() - 1);
        sb.append(") VALUES(");
        for (String key : values.keySet()) {
            sb.append("?,");
        }
        sb.setLength(sb.length() - 1);
        sb.append(")");

        boolean first = true;
        sb.append(" ON DUPLICATE KEY UPDATE ");
        for (String name : values.keySet()) {
            if (ids.contains(name)) continue;
            if (!first) sb.append(",");
            first = false;
            sb.append(name);
            sb.append("=?");
        }
        if (first) return -1;

        String sql = sb.toString();
        log.trace("preSql:" + sql);

        checkConnection();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);

            int i = 1;
            for (String key : values.keySet()) {
                Object o = values.get(key);
                valueMapper(i, o, ps);
                i++;
            }
            for (String key : values.keySet()) {
                if (ids.contains(key)) continue;
                Object o = values.get(key);
                valueMapper(i, o, ps);
                i++;
            }
            return executeUpdate(ps);
        } catch (SQLException e) {
            if (ps != null) log.error("sql:" + ps.toString());
            throw e; //если ошибка пробрасывается выше, то не создаем алерты
        } finally {
            if (ps != null) ps.close();
        }

    }

    @SuppressWarnings("Duplicates")
    public String getMergeSql(DBModel dbModel) {
        Set<String> ids = new HashSet<>();
        Set<String> transients = new HashSet<>();
        String tableName = "";

        try {
            String tableNameTmp = dbModel.getClass().getAnnotation(Table.class).name();
            if (!tableNameTmp.equals("")) tableName = tableNameTmp;
        } catch (Exception e) {
            log.debug("Table not set in class:" + dbModel.getClass().getName());
        }

        try {
            for (Field field : dbModel.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(Id.class)) {
                    ids.add(field.getName());
                }
                if (field.isAnnotationPresent(Transient.class)){
                    transients.add(field.getName());
                }
            }
        } catch (Exception e) {
            log.warn("Error set ids in class:" + dbModel.getClass().getName());
        }

        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ").append(tableName).append("(");
        Map<String, Object> values = new LinkedHashMap<>();
        Method[] methods = dbModel.getClass().getDeclaredMethods();
        for (Method method : methods) {
            String mname = method.getName();
            if (!mname.startsWith("get") && !mname.startsWith("is")) {
                continue;
            }
            String columnName = null;
            if (mname.startsWith("get")) {
                columnName = mname.replaceFirst("get", "").toLowerCase();
            } else if (mname.startsWith("is")) {
                columnName = mname.replaceFirst("is", "").toLowerCase();
            }

            if (transients.contains(columnName)){
                continue;
            }

            Type pType = method.getReturnType();
            if (pType == null) {
                continue;
            }
            try {
                method.setAccessible(true);
                Object result = method.invoke(dbModel);
                if (result != null) {
                    sb.append(columnName).append(",");
                    values.put(columnName, result);
                }

            } catch (Exception e) {
                String desc = "InternalError - Error create model. Error invoke:" + columnName;
                log.error(desc, e);
            }

        }

        if (values.isEmpty()) return null;

        sb.setLength(sb.length() - 1);
        sb.append(") VALUES(");
        for (String key : values.keySet()) {
            Object o = values.get(key);
            sb.append(getStringValue(o));
            sb.append(",");
        }
        sb.setLength(sb.length() - 1);
        sb.append(")");

        boolean first = true;
        sb.append(" ON DUPLICATE KEY UPDATE ");
        for (String name : values.keySet()) {
            if (ids.contains(name)) continue;
            if (!first) sb.append(",");
            first = false;
            sb.append(name);
            sb.append("=");
            sb.append(getStringValue(values.get(name)));
        }
        if (first) return null;
        return sb.toString();
    }

    @SuppressWarnings("Duplicates")
    public String getMergeSqlH2(DBModel dbModel) {
        String tableName = "";

        try {
            String tableNameTmp = dbModel.getClass().getAnnotation(Table.class).name();
            if (!tableNameTmp.equals("")) tableName = tableNameTmp;
        } catch (Exception e) {
            log.debug("Table not set in class:" + dbModel.getClass().getName());
        }

        StringBuilder sb = new StringBuilder();
        sb.append("MERGE INTO ").append(tableName).append("(");
        Map<String, Object> values = new LinkedHashMap<>();
        Method[] methods = dbModel.getClass().getDeclaredMethods();
        for (Method method : methods) {
            String mname = method.getName();
            if (!mname.startsWith("get") && !mname.startsWith("is")) {
                continue;
            }
            String columnName = null;
            if (mname.startsWith("get")) {
                columnName = mname.replaceFirst("get", "").toLowerCase();
            } else if (mname.startsWith("is")) {
                columnName = mname.replaceFirst("is", "").toLowerCase();
            }

            Type pType = method.getReturnType();
            if (pType == null) {
                continue;
            }
            try {
                method.setAccessible(true);
                Object result = method.invoke(dbModel);
                if (result != null) {
                    sb.append(columnName).append(",");
                    values.put(columnName, result);
                }

            } catch (Exception e) {
                String desc = "InternalError - Error create model. Error invoke:" + columnName;
                log.error(desc, e);
            }

        }

        if (values.isEmpty()) return null;

        sb.setLength(sb.length() - 1);
        sb.append(") VALUES(");
        for (String key : values.keySet()) {
            Object o = values.get(key);
            sb.append(getStringValue(o));
            sb.append(",");
        }
        sb.setLength(sb.length() - 1);
        sb.append(")");
        return sb.toString();
    }

    public String getStringValue(Object value) {
        switch (value.getClass().getName()) {
            case "java.lang.String":
                if (value instanceof String) return "'" + value + "'";
                else throw new ClassCastException("Value is not this class!");
            case "boolean":
            case "java.lang.Boolean":
                if (value instanceof Boolean) return value + "";
                else throw new ClassCastException("Value is not this class!");
            case "java.lang.Byte": //what doing with byte[]?
            case "byte": //what doing with byte[]?
                if (value instanceof Byte) return value + "";
                else throw new ClassCastException("Value is not this class!");
            case "java.lang.Short":
            case "short":
                if (value instanceof Short) return value + "";
                else throw new ClassCastException("Value is not this class!");
            case "int":
            case "java.lang.Integer":
                if (value instanceof Integer) return value + "";
                else throw new ClassCastException("Value is not this class!");
            case "long":
            case "java.lang.Long":
                if (value instanceof Long) return value + "";
                else throw new ClassCastException("Value is not this class!");
            case "float":
            case "java.lang.Float":
                if (value instanceof Float) return value + "";
                else throw new ClassCastException("Value is not this class!");
            case "double":
            case "java.lang.Double":
                if (value instanceof Double) return value + "";
                else throw new ClassCastException("Value is not this class!");
            case "java.sql.Date":
                if (value instanceof Date) return "'" + value + "'";
                else throw new ClassCastException("Value is not this class!");
            case "java.sql.Time":
                if (value instanceof Time) return "'" + value + "'";
                else throw new ClassCastException("Value is not this class!");
            case "java.sql.Timestamp":
                if (value instanceof Timestamp) return "'" + value + "'";
                else throw new ClassCastException("Value is not this class!");
            case "java.io.InputStream":
                if (value instanceof InputStream) return value + "";
                else throw new ClassCastException("Value is not this class!");
            default:
                throw new RuntimeException("Unsupported class! " + value.getClass().getName());
        }
    }

    /**
     * DANGEROUS METHOD! DONT USE IT WITHOUT DEEP ANALISE!
     *
     * @param dbModel   Entity for db insert
     * @return executeUpdate result
     * @throws SQLException
     */
    public int insertModel(DBModel dbModel) throws SQLException {
        if (dbModel == null) return -1;
        Set<String> ids = new HashSet<>();
        Set<String> transients = new HashSet<>();
        String tableName = "";

        try {
            String tableNameTmp = dbModel.getClass().getAnnotation(Table.class).name();
            if (!tableNameTmp.equals("")) tableName = tableNameTmp;
        } catch (Exception e) {
            log.debug("Table not set in class:" + dbModel.getClass().getName());
        }

        try {
            for (Field field : dbModel.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(Id.class)) {
                    ids.add(field.getName());
                }
                if (field.isAnnotationPresent(Transient.class)) {
                    transients.add(field.getName());
                }
            }
        } catch (Exception e) {
            log.warn("Error set ids in class:" + dbModel.getClass().getName());
        }

        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ").append(tableName).append("(");
        Map<String, Object> values = new LinkedHashMap<>();
        Method[] methods = dbModel.getClass().getDeclaredMethods();
        for (Method method : methods) {
            String mname = method.getName();
            if (!mname.startsWith("get") && !mname.startsWith("is")) {
                continue;
            }
            String columnName = null;
            if (mname.startsWith("get")) {
                columnName = mname.replaceFirst("get", "").toLowerCase();
            } else if (mname.startsWith("is")) {
                columnName = mname.replaceFirst("is", "").toLowerCase();
            }

            if (ids.contains(columnName) || transients.contains(columnName)) continue;

            Type pType = method.getReturnType();
            if (pType == null) {
                continue;
            }
            try {
                method.setAccessible(true);
                Object result = method.invoke(dbModel);
                if (result != null) {
                    sb.append(columnName).append(",");
                    values.put(columnName, result);
                }

            } catch (Exception e) {
                String desc = "InternalError - Error create model. Error invoke:" + columnName;
                log.error(desc, e);
            }

        }

        if (values.isEmpty()) return -1;

        sb.setLength(sb.length() - 1);
        sb.append(") VALUES(");
        for (String key : values.keySet()) {
            sb.append("?").append(",");
        }
        sb.setLength(sb.length() - 1);
        sb.append(")");


        checkConnection();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sb.toString());

            int i = 1;
            for (String key : values.keySet()) {
                Object o = values.get(key);
                valueMapper(i, o, ps);
                i++;
            }
            return executeUpdate(ps);
        } catch (SQLException e) {
            if (ps != null) log.error("sql:" + ps.toString());
            throw e; //если ошибка пробрасывается выше, то не создаем алерты
        } finally {
            if (ps != null) ps.close();
        }
    }

    public void truncate(String name) throws SQLException {
        PreparedStatement ps = conn.prepareStatement("TRUNCATE " + name);
        executeUpdate(ps);
    }

    public void deleteAll(String name) throws SQLException {
        PreparedStatement ps = conn.prepareStatement("DELETE FROM " + name);
        executeUpdate(ps);
    }

}
