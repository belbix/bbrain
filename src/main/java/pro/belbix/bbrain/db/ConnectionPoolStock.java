/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pro.belbix.bbrain.db;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.utils.Config;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPoolStock {
    private static final Logger log = LogManager.getRootLogger();
    private static BasicDataSource ds;

    private static class SingletonHolder {
        private static final ConnectionPoolStock HOLDER_INSTANCE = new ConnectionPoolStock();
    }

    public static ConnectionPoolStock getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    private ConnectionPoolStock() {
        openPool();
    }

    private void openPool() {
        ds = new BasicDataSource();
        ds.setDriverClassName(Config.stock_db_class);
        ds.setUsername(Config.stock_db_user);
        ds.setPassword(Config.stock_db_password);
        ds.setUrl(Config.stock_db_url);
        ds.setInitialSize(Config.stock_db_initial_size);
        ds.setMaxIdle(Config.stock_db_max_idle);
        ds.setMaxTotal(Config.stock_db_max_total);
        ds.setMinIdle(Config.stock_db_min_idle);

        ds.setPoolPreparedStatements(true);
        ds.setMaxOpenPreparedStatements(500);
        ds.setDefaultAutoCommit(true);

        ds.setTimeBetweenEvictionRunsMillis(5000);
        ds.setRemoveAbandonedOnMaintenance(true);
        ds.setRemoveAbandonedTimeout(2000);

        ds.setValidationQuery("SELECT 1");
        ds.setValidationQueryTimeout(5);

        ds.setTestWhileIdle(true);
        log.debug("ConnectionPoolStock activated.");
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public void putConnection(Connection connection) throws SQLException {
        connection.close();
    }

    public void closePool(){
        try {
            ds.close();
        } catch (SQLException e) {
            log.error("Error close pool", e);
        }
    }
}
