package pro.belbix.bbrain.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

/**
 * Created by v.belykh on 07.08.2018
 */
public class DBLayerStock extends DBLayer{
    private static final Logger log = LogManager.getRootLogger();

    @Override
    public void openConnection() throws SQLException {
        if (conn == null || conn.isClosed()) {
            conn = ConnectionPoolStock.getInstance().getConnection();
        } else {
            log.debug("Connection already opened!");
        }
    }
}
