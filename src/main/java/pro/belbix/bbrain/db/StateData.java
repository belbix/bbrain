package pro.belbix.bbrain.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by v.belykh on 11.07.2018
 */
public class StateData {
    private static final Logger log = LogManager.getRootLogger();
    private DBLayer dbLayer = null;

    public StateData(DBLayer dbLayer) {
        this.dbLayer = dbLayer;
    }


    private static final String SELECT_LAYERS = "SELECT * FROM layers";
    private static final String SELECT_NEURONS = "SELECT * FROM neurons";
    private static final String SELECT_AXONS = "SELECT * FROM axons";
    private static final String SELECT_DENDITES = "SELECT * FROM dendrites";
    private static final String SELECT_EPS = "SELECT * FROM eps";
    private static final String SELECT_RECEIVERS = "SELECT * FROM receivers";
    private static final String SELECT_RECEPTORS = "SELECT * FROM receptors";
    private static final String SELECT_SENDERS = "SELECT * FROM senders";
    private static final String SELECT_TRANSMITTERS = "SELECT * FROM transmitters";
    private static final String SELECT_ALL = "SELECT * FROM layers" +
            "  LEFT JOIN neurons ON (neurons.layer_id = layers.id)" +
            "  LEFT JOIN manipulators ON (manipulators.layer_id = layers.id)" +
            "  LEFT JOIN eps ON (neurons.id = eps.neuron_id AND neurons.layer_id = eps.layer_id)" +
            "  LEFT JOIN axons ON (neurons.id = axons.neuron_id AND neurons.layer_id = axons.layer_id)" +
            "  LEFT JOIN senders ON (neurons.id = senders.neuron_id AND neurons.layer_id = senders.layer_id AND senders.axon_id = axons.id)" +
            "  LEFT JOIN transmitters ON (neurons.id = transmitters.neuron_id AND neurons.layer_id = transmitters.layer_id AND transmitters.axon_id = axons.id AND transmitters.sender_id = senders.id)" +
            "  LEFT JOIN dendrites ON (neurons.id = dendrites.neuron_id AND neurons.layer_id = dendrites.layer_id)" +
            "  LEFT JOIN receivers ON (neurons.id = receivers.neuron_id AND neurons.layer_id = receivers.layer_id AND receivers.dendrite_id = dendrites.id)" +
            "  LEFT JOIN receptors ON (neurons.id = receptors.neuron_id AND neurons.layer_id = receptors.layer_id AND receptors.dendrite_id = dendrites.id AND receptors.receiver_id = receivers.id)";
    private PreparedStatement psSelectLayers = null;
    private PreparedStatement psSelectNeurons = null;
    private PreparedStatement psSelectAxons = null;
    private PreparedStatement psSelectDendrites = null;
    private PreparedStatement psSelectEps = null;
    private PreparedStatement psSelectReceivers = null;
    private PreparedStatement psSelectReceptors = null;
    private PreparedStatement psSelectSenders = null;
    private PreparedStatement psSelectTransmitters = null;
    private PreparedStatement psSelectAll = null;

    public void initPS() throws SQLException {
        Connection conn = dbLayer.getConn();

        psSelectLayers = conn.prepareStatement(SELECT_LAYERS);
        psSelectNeurons = conn.prepareStatement(SELECT_NEURONS);
        psSelectAxons = conn.prepareStatement(SELECT_AXONS);
        psSelectDendrites = conn.prepareStatement(SELECT_DENDITES);
        psSelectEps = conn.prepareStatement(SELECT_EPS);
        psSelectReceivers = conn.prepareStatement(SELECT_RECEIVERS);
        psSelectReceptors = conn.prepareStatement(SELECT_RECEPTORS);
        psSelectSenders = conn.prepareStatement(SELECT_SENDERS);
        psSelectTransmitters = conn.prepareStatement(SELECT_TRANSMITTERS);
        psSelectAll = conn.prepareStatement(SELECT_ALL);
    }

    public void close() {
        try {
            psSelectLayers.close();
            psSelectNeurons.close();
            psSelectAxons.close();
            psSelectDendrites.close();
            psSelectEps.close();
            psSelectReceivers.close();
            psSelectReceptors.close();
            psSelectSenders.close();
            psSelectTransmitters.close();
            psSelectAll.close();
        } catch (SQLException e) {
            log.error("", e);
        }

        dbLayer.close();
    }

    public void clearEP() {
        log.info("clearEP");
        try {
            dbLayer.deleteAll("eps");
        } catch (SQLException e) {
            log.error("Error clearEP", e);
        }
    }

    public void clearBrain() {
        log.info("Clear neuron data in DB");
        try {
            dbLayer.deleteAll("eps");
            dbLayer.deleteAll("transmitters");
            dbLayer.deleteAll("receptors");
            dbLayer.deleteAll("receivers");
            dbLayer.deleteAll("senders");
            dbLayer.deleteAll("axons");
            dbLayer.deleteAll("dendrites");
            dbLayer.deleteAll("neurons");
            dbLayer.deleteAll("manipulators");
        } catch (SQLException e) {
            log.error("Error clearBrain", e);
        }
    }


}
