package pro.belbix.bbrain.stock;

/**
 * Created by v.belykh on 9/25/2018
 */
public class Price {

    private Double value;
    private Double minValue;
    private Double maxValue;

    public Price(Double value, Double minValue, Double maxValue) {
        this.value = value;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public String toString() {
        return "Price{" +
                (value != null ? "value=" + value + "," : "") +
                (minValue != null ? "minValue=" + minValue + "," : "") +
                (maxValue != null ? "maxValue=" + maxValue + "," : "") +
                '}';
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getMinValue() {
        return minValue;
    }

    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

}
