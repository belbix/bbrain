package pro.belbix.bbrain.stock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.db.DBLayer;
import pro.belbix.bbrain.models.Candle;
import pro.belbix.bbrain.models.Charge;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Created by v.belykh on 11.07.2018
 */
public class StockData {
    private static final Logger log = LogManager.getRootLogger();
    private DBLayer dbLayer = null;

    public StockData(DBLayer dbLayer) {
        this.dbLayer = dbLayer;
    }

    private static final String SELECT_CANDLES =
            "SELECT * FROM crypto.candles WHERE server = ? and symbol = ? and time = ? and date <= ? ORDER BY date DESC LIMIT ?";

    public void insertCharge(Charge charge) throws SQLException {
        dbLayer.checkConnection();
        dbLayer.insertModel(charge);
    }

    public ArrayList<Candle> getCandles(String server, String symbol, int time, LocalDateTime fromDate, int limit) throws SQLException {
        dbLayer.checkConnection();
        ArrayList<Candle> candles = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement ps = dbLayer.getConn().prepareStatement(SELECT_CANDLES);
        try {
            ps.setString(1, server);
            ps.setString(2, symbol);
            ps.setInt(3, time);
            ps.setTimestamp(4, Timestamp.valueOf(fromDate));
            ps.setInt(5, limit);
            rs = dbLayer.executeStatement(ps);
            while (rs.next()) {
                Candle c = new Candle();
                DBLayer.createModelFromRs(rs, c);
                candles.add(c);
            }
        } catch (SQLException e) {
            log.error("Error in sql:" + ps.toString(), e);
            throw e;
        } finally {
            if (rs != null) rs.close();
            if (ps != null) ps.close();
        }
        return candles;
    }
}
