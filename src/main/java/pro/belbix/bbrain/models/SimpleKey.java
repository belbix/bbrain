package pro.belbix.bbrain.models;

import java.math.BigInteger;

/**
 * Created by v.belykh on 17.04.2018
 */
public class SimpleKey implements Key {
    private String str1;
    private String str2;
    private String str3;
    private long long1;
    private long long2;
    private long long3;
    private long long4;
    private long long5;
    private long long6;
    private BigInteger bigInteger = null;



    public String getStr1() {
        return str1;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public String getStr2() {
        return str2;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }

    public String getStr3() {
        return str3;
    }

    public void setStr3(String str3) {
        this.str3 = str3;
    }

    public long getLong1() {
        return long1;
    }

    public void setLong1(long long1) {
        this.long1 = long1;
    }

    public long getLong2() {
        return long2;
    }

    public void setLong2(long long2) {
        this.long2 = long2;
    }

    public long getLong3() {
        return long3;
    }

    public void setLong3(long long3) {
        this.long3 = long3;
    }

    public long getLong4() {
        return long4;
    }

    public void setLong4(long long4) {
        this.long4 = long4;
    }

    public long getLong5() {
        return long5;
    }

    public void setLong5(long long5) {
        this.long5 = long5;
    }

    public long getLong6() {
        return long6;
    }

    public void setLong6(long long6) {
        this.long6 = long6;
    }

    public BigInteger getBigInteger() {
        return bigInteger;
    }

    public void setBigInteger(BigInteger bigInteger) {
        this.bigInteger = bigInteger;
    }

    @Override
    public String toString() {
        return "SimpleKey{" +
                (str1 != null ? "str1='" + str1 + '\'' + "," : "") +
                (str2 != null ? "str2='" + str2 + '\'' + "," : "") +
                (str3 != null ? "str3='" + str3 + '\'' + "," : "") +
                "long1='" + long1 + '\'' + "," +
                "long2='" + long2 + '\'' + "," +
                "long3='" + long3 + '\'' + "," +
                "long4='" + long4 + '\'' + "," +
                "long5='" + long5 + '\'' + "," +
                "long6='" + long6 + '\'' + "," +
                (bigInteger != null ? "bigInteger=" + bigInteger + "," : "") +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleKey)) return false;

        SimpleKey key = (SimpleKey) o;

        if (long1 != key.long1) return false;
        if (long2 != key.long2) return false;
        if (long3 != key.long3) return false;
        if (long4 != key.long4) return false;
        if (long5 != key.long5) return false;
        if (long6 != key.long6) return false;
        if (str1 != null ? !str1.equals(key.str1) : key.str1 != null) return false;
        if (str2 != null ? !str2.equals(key.str2) : key.str2 != null) return false;
        if (str3 != null ? !str3.equals(key.str3) : key.str3 != null) return false;
        return bigInteger != null ? bigInteger.equals(key.bigInteger) : key.bigInteger == null;
    }

    @Override
    public int hashCode() {
        int result = str1 != null ? str1.hashCode() : 0;
        result = 31 * result + (str2 != null ? str2.hashCode() : 0);
        result = 31 * result + (str3 != null ? str3.hashCode() : 0);
        result = 31 * result + (int) (long1 ^ (long1 >>> 32));
        result = 31 * result + (int) (long2 ^ (long2 >>> 32));
        result = 31 * result + (int) (long3 ^ (long3 >>> 32));
        result = 31 * result + (int) (long4 ^ (long4 >>> 32));
        result = 31 * result + (int) (long5 ^ (long5 >>> 32));
        result = 31 * result + (int) (long6 ^ (long6 >>> 32));
        result = 31 * result + (bigInteger != null ? bigInteger.hashCode() : 0);
        return result;
    }
}
