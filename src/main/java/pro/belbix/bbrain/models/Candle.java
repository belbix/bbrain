package pro.belbix.bbrain.models;

/**
 * Created by v.belykh on 25.05.2018
 */
@Table(name = "candles")
public class Candle implements DBModel {

    @Id
    private Long id;
    @Transient
    private String name;
    private String server;
    private String symbol;
    private Long time;
    private java.sql.Timestamp date;
    private Double amount;
    private Double close;
    private Double high;
    private Double low;
    private Double open;
    private Double volume;
    private Double slowk;
    private Double slowd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public java.sql.Timestamp getDate() {
        return date;
    }

    public void setDate(java.sql.Timestamp date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getClose() {
        return close;
    }

    public void setClose(Double close) {
        this.close = close;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getSlowk() {
        return slowk;
    }

    public void setSlowk(Double slowk) {
        this.slowk = slowk;
    }

    public Double getSlowd() {
        return slowd;
    }

    public void setSlowd(Double slowd) {
        this.slowd = slowd;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return close + "";
    }
}
