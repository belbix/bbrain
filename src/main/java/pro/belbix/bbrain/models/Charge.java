package pro.belbix.bbrain.models;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by v.belykh on 10/10/2018
 */
@Table(name = "charges")
public class Charge implements DBModel{
    @Id
    private Long id;
    @Transient
    private String name;
    private String server;
    private String symbol;
    private java.sql.Timestamp date_create = Timestamp.valueOf(LocalDateTime.now());
    private Long type;
    private Double price;
    private Long candle_id;
    private Long strategy_id;
    private Long init_charge;
    private Long close_position;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Timestamp getDate_create() {
        return date_create;
    }

    public void setDate_create(Timestamp date_create) {
        this.date_create = date_create;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getCandle_id() {
        return candle_id;
    }

    public void setCandle_id(Long candle_id) {
        this.candle_id = candle_id;
    }

    public Long getStrategy_id() {
        return strategy_id;
    }

    public void setStrategy_id(Long strategy_id) {
        this.strategy_id = strategy_id;
    }

    public Long getInit_charge() {
        return init_charge;
    }

    public void setInit_charge(Long init_charge) {
        this.init_charge = init_charge;
    }

    public Long getClose_position() {
        return close_position;
    }

    public void setClose_position(Long close_position) {
        this.close_position = close_position;
    }

    @Override
    public String getName() {
        return "";
    }
}
