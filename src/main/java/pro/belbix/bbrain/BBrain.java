package pro.belbix.bbrain;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.processing.*;
import pro.belbix.bbrain.utils.Config;
import pro.belbix.bbrain.visual.BBVisual;

/**
 * Created by v.belykh on 25.04.2018
 */
public class BBrain {
    private static final Logger log = LogManager.getRootLogger();
    private static final int THREADS = 1;
    private static boolean candleProcessor = false;
    private static boolean xorProcessor = true;

    public static void main(String[] args) {
        log.info("Start BBrain");
        log.info("ProcessingCenter.initLayers");
        ProcessingCenter.initLayers();

        if(candleProcessor) {
            CandleProcessor candleProcessor = new CandleProcessor();
            candleProcessor.setCandleNeurons(ProcessingCenter.getReceptors());
            candleProcessor.setManipulators(ProcessingCenter.getManipulators());
            log.info("Start CandleProcessor");
            startThread(candleProcessor, "CandleProcessor");
        }

        if(xorProcessor){
            XorProcessor xorProcessor = new XorProcessor();
            xorProcessor.setReceptorsNeurons(ProcessingCenter.getReceptors());
            xorProcessor.setManipulators(ProcessingCenter.getManipulators());
            log.info("Start XorProcessor");
            startThread(xorProcessor, "XorProcessor");
        }



        log.info("Start NeuronHandlers");
        for (int i = 0; i < THREADS; i++) {
            startThread(new NeuronHandler(), "NeuronHandler" + i);
        }

        if (Config.useLocalGraphic) {
            log.info("Start BBVisual");
            new Thread(new BBVisual()).start();
        }

        startThread(new NeuronRefresher(), "NeuronRefresher");

        log.info("BBrain started");
    }

    private static void startThread(Runnable runnable, String name) {
        Thread t = new Thread(runnable);
        t.setName(name);
        t.start();
    }
}
