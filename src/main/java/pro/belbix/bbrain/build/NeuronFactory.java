package pro.belbix.bbrain.build;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.neuron.*;
import pro.belbix.bbrain.utils.Config;

import java.time.Instant;
import java.util.List;
import java.util.Random;
import java.util.Set;

import static pro.belbix.bbrain.neuron.Synapse.Type.ACTIVATOR;
import static pro.belbix.bbrain.neuron.Synapse.Type.INHIBITOR;

/**
 * Created by v.belykh on 23.05.2018
 */
public class NeuronFactory {
    private static final Logger log = LogManager.getRootLogger();
    private static Random random = new Random();
    private final static int Z_INCREMENT = -1;

    public static void createNeuronFromLayer(Layer layer) {
        for (int neuronId = 0; neuronId < layer.getNeuronCount(); neuronId++) {
            Neuron neuron = new Neuron();
            layer.getNeurons().put((long) neuronId, neuron);
            neuron.setLayer(layer);
            neuron.setId(neuronId);

            for (int dendriteId = 0; dendriteId < layer.getDendriteCount(); dendriteId++) {
                Dendrite dendrite = new Dendrite(neuron);
                dendrite.setId(neuron.nextDendriteId());

//                for (int receiverId = 0; receiverId < layer.getReceiverCount(); receiverId++) {
//                    addReceiver(dendrite, randomSynapse(layer.getDendriteSynapses()));
//                }

            }

            for (int axonId = 0; axonId < layer.getAxonCount(); axonId++) {
                Axon axon = new Axon(neuron);
                axon.setId(axonId);
                axon.setTimeNeedToActivation(layer.getAxonTimeNeedToActivation());

                for (int senderId = 0; senderId < layer.getSenderCount(); senderId++) {
                    addSender(axon, randomSynapse(layer.getAxonSynapses()));
                }
            }

            switch (layer.getLayerType()){
                case RECEPTOR:
                case MANIPULATOR:
                case PLEASURE:
                case PUNISHMENT:
                    neuron.setReadyToShow(true);
                    break;
            }
        }



        log.info(layer.getName() + " create:" + layer.getNeurons().size());
    }


    public static void calcCoordinates(Neuron neuron, MindBox mindBox) {
        Layer layer = neuron.getLayer();
        float neuronCount = (float) layer.getNeuronCount();
        float startPointX;
        if (mindBox.getMaxX() < neuronCount) {
            mindBox.setMaxX(neuronCount);
            startPointX = 0;
        } else {
            startPointX = (mindBox.getMaxX() - neuronCount) / 2;
        }
        float x = startPointX + neuron.getId();
        float y = layer.getY();
        float z = getZ(x, y, 0, mindBox);

        neuron.setX(x);
        neuron.setY(y);
        neuron.setZ(z);
    }

    private static float getZ(float x, float y, float z, MindBox mindBox) {
        if (!mindBox.addCoordinate(x, y, z)) {
            z += Z_INCREMENT;
            return getZ(x, y, z, mindBox);
        }
        return z;
    }

    public static Neuron createEmptyNeuron(int id) {
        Neuron neuron = new Neuron();
        neuron.setId(id);
        neuron.setLastSpike(null);
        neuron.setRefractoryResist(0L);
        Layer layer = new Layer();
        neuron.setLayer(layer);
        layer.getNeurons().put(neuron.getId(), neuron);
        return neuron;
    }

    public static Neuron createEmptyNeuron(int id, Layer layer) {
        Neuron neuron = new Neuron();
        neuron.setId(id);
        neuron.setLastSpike(null);
        neuron.setRefractoryResist(0L);
        neuron.setLayer(layer);
        layer.getNeurons().put(neuron.getId(), neuron);
        return neuron;
    }

    public static Receptor createReceptor(int id, Receiver receiver, Synapse synapse, long energy) {
        Receptor receptor = new Receptor(receiver);
        receptor.setId(id);
        receptor.setEnergy(energy);
        receptor.setSynapse(synapse);
        receiver.addReceptor(receptor);
        return receptor;
    }

    public static void createReceptorWithAnySynapse(Receiver receiver, Set<Synapse.Type> transmitterTypes) {
        for (Synapse.Type type : transmitterTypes) {
            boolean found = false;
            for (Receptor receptor : receiver.getReceptors()) {
                if (receptor.getSynapse().getType().equals(type)) {
                    found = true;
                    break;
                }
            }
            if (found) continue;
            NeuronFactory.createReceptor(receiver.getReceptors().size(), receiver, new Synapse(type), Config.receiverReceptorDefaultEnergy);
        }
    }

    public static Receiver createReceiver(long id, Dendrite dendrite) {
        Receiver receiver = new Receiver(dendrite);
        receiver.setId(id);
        dendrite.addReceiver(receiver);
        return receiver;
    }

    public static Sender createEmptySender(int id, Axon axon) {
        Sender sender = new Sender(axon);
        sender.setId(id);
        axon.addSender(sender);
        return sender;
    }

    public static Transmitter createTransmitter(int id, Sender sender, Synapse synapse) {
        Transmitter transmitter = new Transmitter();
        transmitter.setSynapse(synapse);
        transmitter.setId(id);
        transmitter.setCount(Config.transmitterDefaultCount);
        transmitter.setNeedToActivation(Config.transmitterDefaultNeedActivation);
        transmitter.setSender(sender);
        sender.addTransmitters(transmitter);
        return transmitter;
    }

    public static Receiver addReceiver(Dendrite dendrite, Synapse synapse) {
        Receiver receiver = new Receiver(dendrite);
        Layer layer = dendrite.getNeuron().getLayer();
        Receptor receptor = new Receptor(receiver);
        receptor.setSynapse(synapse);
        receptor.setId(0); //будем считать его дефолтным, а последущие уже создаются в рантайме
        long energy = 0;
        switch (synapse.getType()) {
            case ACTIVATOR:
            case INHIBITOR:
                energy = layer.getReceptorEnergy();
                break;
        }
        receptor.setEnergy(energy);

        receiver.addReceptor(receptor);
        receiver.setId(dendrite.nextReceiverId());
        dendrite.addReceiver(receiver);
        return receiver;
    }

    public static Synapse randomSynapse() {
        Synapse.Type type;
        if (random.nextFloat() < Config.synapseRandom) {
            type = ACTIVATOR;
        } else {
            type = INHIBITOR;
        }
        return new Synapse(type);
    }

    public static Synapse randomSynapse(List<Synapse.Type> types) {
        if(types.isEmpty()) return null;
        int i = Math.round(random.nextFloat() * (types.size() - 1));
        Synapse.Type type = types.get(i);
        if(type == null) return null;
        return new Synapse(type);
    }

    public static void addSender(Axon axon, Synapse synapse) {
        if(axon == null || synapse == null) {
            log.info("Cant add sender");
            return;
        }
        Sender sender = new Sender(axon);
        Layer layer = axon.getNeuron().getLayer();
        axon.addSender(sender);

        Transmitter transmitter = new Transmitter();
        transmitter.setSynapse(synapse);
        transmitter.setId(0); //будем считать его дефолтным, а последущие уже создаются в рантайме
        transmitter.setCount((int) layer.getTransmitterCount());
        transmitter.setNeedToActivation((int) layer.getTransmitterNeedActivation());
        transmitter.setModifyReceptorPowerValue((int)layer.getTransmitterModifyReceptorPower());
        transmitter.setSender(sender);
        sender.addTransmitters(transmitter);
    }

    public static ElectricalPotential createDefaultEP(double value, Neuron neuron, long id) {
        ElectricalPotential ep = new ElectricalPotential((long) (value * (double) (Config.receiverReceptorDefaultEnergy * Config.transmitterDefaultCount)));
        ep.setInitTime(Instant.now());
        ep.setId(id);
        ep.setFromReceiver(0L);
        ep.setFromDendrite(0L);
        ep.setNeuron(neuron);
        return ep;
    }

}
