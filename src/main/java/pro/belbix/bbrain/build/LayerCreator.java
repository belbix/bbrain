package pro.belbix.bbrain.build;

import pro.belbix.bbrain.neuron.Layer;
import pro.belbix.bbrain.neuron.Synapse;
import pro.belbix.bbrain.utils.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import static pro.belbix.bbrain.neuron.Layer.LayerType.*;
import static pro.belbix.bbrain.neuron.Synapse.Type.*;

/**
 * Created by v.belykh on 10/5/2018
 */
public class LayerCreator {
    private static LayerCreator ourInstance = new LayerCreator();

    public static LayerCreator getInstance() {
        return ourInstance;
    }

    private LayerCreator() {
    }

    private LinkedList<Layer> layers = new LinkedList<>();

    public void init() {
        Layer receptorLayer = new Layer();
        receptorLayer.setLayerType(RECEPTOR);
        receptorLayer.setId((long) layers.size());
        receptorLayer.setY(layers.size());
        receptorLayer.setDendriteCount(0);
        receptorLayer.setSenderCount(3);
        receptorLayer.setSenderMax(3);
        receptorLayer.setName("ReceptorLayer");
        receptorLayer.setNeuronCount(Config.neuronCount);
        layers.add(receptorLayer);

        int associativeNeuronCount = (int) (Config.neuronCount * 1.5);

        Layer associativeLayer = new Layer();
        associativeLayer.setLayerType(ASSOCIATIVE);
        associativeLayer.setId((long) layers.size());
        associativeLayer.setY(layers.size());
        associativeLayer.setName("AssociativeLayer");
        associativeLayer.setNeuronCount(associativeNeuronCount);
//        associativeLayer.setDendriteCount(3);
        associativeLayer.setReceiverCount(3);
        associativeLayer.setSenderCount(5);
        associativeLayer.setSenderMax(5);
        layers.add(associativeLayer);


        Layer manipulatorLayer = new Layer();
        manipulatorLayer.setLayerType(MANIPULATOR);
        manipulatorLayer.setId((long) layers.size());
        manipulatorLayer.setY(layers.size());
        manipulatorLayer.setName("ManipulatorLayer");
        manipulatorLayer.setNeuronCount(1);
        manipulatorLayer.setAxonCount(0);
        manipulatorLayer.setSenderCount(0);
        manipulatorLayer.setSenderMax(0);
        manipulatorLayer.setDendriteCount(1);
        manipulatorLayer.setReceiverCount(3);
        manipulatorLayer.setImmutable(true);
        layers.add(manipulatorLayer);

        int senders = associativeNeuronCount + 1;
        Layer pleasureLayer = new Layer();
        pleasureLayer.setLayerType(PLEASURE);
        pleasureLayer.setId((long) layers.size());
        pleasureLayer.setY(manipulatorLayer.getY());
        pleasureLayer.setName("PleasureLayer");
        pleasureLayer.setNeuronCount(1);
        pleasureLayer.setAxonCount(1);
        pleasureLayer.setSenderCount(senders);
        pleasureLayer.setSenderMax(senders);
        pleasureLayer.setDendriteCount(0);
        pleasureLayer.setReceiverCount(0);
        layers.add(pleasureLayer);

        Layer punishmentLayer = new Layer();
        punishmentLayer.setLayerType(PUNISHMENT);
        punishmentLayer.setId((long) layers.size());
        punishmentLayer.setY(manipulatorLayer.getY());
        punishmentLayer.setName("PunishmentLayer");
        punishmentLayer.setNeuronCount(1);
        punishmentLayer.setAxonCount(1);
        punishmentLayer.setSenderCount(senders);
        punishmentLayer.setSenderMax(senders);
        punishmentLayer.setDendriteCount(0);
        punishmentLayer.setReceiverCount(0);
        layers.add(punishmentLayer);
    }

    private Layer initLayer(Layer layer) {

//        switch (layer.getLayerType()) {
//            case RECEPTOR: //must not have dendrites
//                break;
//            case ASSOCIATIVE:
//            case PROCESSING:
//            case MEMORY:
//            case MANIPULATOR:
//            case PLEASURE:
//                layer.setReceiverSynapse(new ArrayList<>(Arrays.asList(
//                        new Synapse(ACTIVATOR)
//                )));
//                break;
//        }

        switch (layer.getLayerType()) {
            case RECEPTOR:
                layer.getAxonSynapses().add(ACTIVATOR);
                break;
            case MANIPULATOR:
                layer.getAxonSynapses().add(ACTIVATOR);
                layer.getDendriteSynapses().add(ACTIVATOR);
                break;
            case PLEASURE:
                layer.getAxonSynapses().add(STIMULATION);
                layer.getDendriteSynapses().add(ACTIVATOR);
                break;
            case PUNISHMENT:
                layer.getAxonSynapses().add(PUNISH);
                layer.getDendriteSynapses().add(ACTIVATOR);
                break;
            default:
                layer.getDendriteSynapses().add(ACTIVATOR);
                layer.getDendriteSynapses().add(INHIBITOR);

                layer.getAxonSynapses().add(ACTIVATOR);
                layer.getAxonSynapses().add(INHIBITOR);
                break;
        }
        return layer;
    }

    public ArrayList<Layer> createLayers() {
        ArrayList<Layer> initedLayers = new ArrayList<>();
        for (Layer layer : layers) {
            initedLayers.add(initLayer(layer));
        }
        return initedLayers;
    }

}
