package pro.belbix.bbrain.build;

import pro.belbix.bbrain.neuron.Receiver;
import pro.belbix.bbrain.neuron.Receptor;
import pro.belbix.bbrain.neuron.Transmitter;

import java.util.ArrayList;

/**
 * Created by v.belykh on 23.05.2018
 */
public class NeuronReceipt {
    private int dendriteCount = 1;
    private int dendriteReceiverCount = 1;
    private ArrayList<Receptor> dendriteReceptors = new ArrayList<>();
    private int axonCount = 1;
    private int axonSenderCount = 1;
    private ArrayList<Receiver> axonOutputReceivers = new ArrayList<>();
    private ArrayList<Transmitter> axonTransmitters = new ArrayList<>();

    public int getDendriteCount() {
        return dendriteCount;
    }

    public void setDendriteCount(int dendriteCount) {
        this.dendriteCount = dendriteCount;
    }

    public int getDendriteReceiverCount() {
        return dendriteReceiverCount;
    }

    public void setDendriteReceiverCount(int dendriteReceiverCount) {
        this.dendriteReceiverCount = dendriteReceiverCount;
    }

    public ArrayList<Receptor> getDendriteReceptors() {
        return dendriteReceptors;
    }

    public void setDendriteReceptors(ArrayList<Receptor> dendriteReceptors) {
        this.dendriteReceptors = dendriteReceptors;
    }

    public int getAxonCount() {
        return axonCount;
    }

    public void setAxonCount(int axonCount) {
        this.axonCount = axonCount;
    }

    public int getAxonSenderCount() {
        return axonSenderCount;
    }

    public void setAxonSenderCount(int axonSenderCount) {
        this.axonSenderCount = axonSenderCount;
    }

    public ArrayList<Receiver> getAxonOutputReceivers() {
        return axonOutputReceivers;
    }

    public void addAxonOutputReceiver(Receiver axonOutputReceiver) {
        this.axonOutputReceivers.add(axonOutputReceiver);
    }

    public ArrayList<Transmitter> getAxonTransmitters() {
        return axonTransmitters;
    }

    public void setAxonTransmitters(ArrayList<Transmitter> axonTransmitters) {
        this.axonTransmitters = axonTransmitters;
    }
}
