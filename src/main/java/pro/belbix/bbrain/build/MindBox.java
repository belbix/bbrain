package pro.belbix.bbrain.build;

import java.util.HashMap;
import java.util.HashSet;

public class MindBox {

    private float maxX;
    private float maxY;
    private float maxZ;

    private HashMap<Float, HashMap<Float, HashSet<Float>>> coordinates = new HashMap<>();

    public boolean addCoordinate(float x, float y, float z){
        HashMap<Float, HashSet<Float>> yz = coordinates.computeIfAbsent(x, k -> new HashMap<>());

        HashSet<Float> zExist = yz.get(y);
        if(zExist == null){
            HashSet<Float> zSet = new HashSet<>();
            zSet.add(z);
            yz.put(y, zSet);
            return true;
        }
        if(zExist.contains(z)) {
            return false;
        } else {
            zExist.add(z);
            return true;
        }
    }

    public void clearCoordinates(){
        coordinates.clear();
    }

    public float getMaxX() {
        return maxX;
    }

    public void setMaxX(float maxX) {
        this.maxX = maxX;
    }

    public float getMaxY() {
        return maxY;
    }

    public void setMaxY(float maxY) {
        this.maxY = maxY;
    }

    public float getMaxZ() {
        return maxZ;
    }

    public void setMaxZ(float maxZ) {
        this.maxZ = maxZ;
    }
}
