package pro.belbix.bbrain.neuron;

import pro.belbix.bbrain.models.Id;
import pro.belbix.bbrain.utils.Config;
import pro.belbix.bbrain.visual.LastValueHolder;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by v.belykh on 25.04.2018
 */
public class Sender {
    // --------------- STATE FIELDS -----------------------
    @Id
    private long id = 0;
    @Id
    private Long layer_id;
    @Id
    private Long axon_id;
    @Id
    private Long neuron_id;
    private String name = null;
    private Destination destination = null;
    private Long receiver_id;
    private Long rlayer_id;
    private Long rdendrite_id;
    private Long rneuron_id;

    //------------- PARENT ----------------------------------
    private Axon axon = null;

    //------------- FOR PROCESSING --------------------------
    private List<Transmitter> transmitters = new CopyOnWriteArrayList<>();

    //------------- NEXT NEURON -----------------------------
    private Receiver receiver = null;

    //------------- LOCAL VARIABLE --------------------------
    private LastValueHolder<Boolean> activated = new LastValueHolder<>(Config.visualisationPause);
    private long wishGetReceiver = 0;
    private Instant lastTransfer = null;
    private Map<Receiver, Integer> receiverCandidates = new ConcurrentHashMap<>();
    //------------- STP --------------------------
    public static final long RESOURCE_FULL = 1000;
    private long potentiationResource = RESOURCE_FULL; //for STD
    public static final long CONSUMPTION_RESOURCE = 100;
    private long potentiationBalance = 0; // for STF
    public static final double DELTA_LOST_POTENTION = 0.1;

    public Sender(Axon axon) {
        this.axon = axon;
        id = axon.nextSenderId();
        layer_id = axon.getNeuron().getLayerId();
        axon_id = axon.getId();
        neuron_id = axon.getNeuron().getId();
    }

    //TODO SHORT TERM POTENTIATION!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //Short Term Facilitation/Depression http://www.scholarpedia.org/article/Short-term_synaptic_plasticity

    private boolean consumptionResource() {
        if(true) return true;
        if (potentiationResource < CONSUMPTION_RESOURCE) return false;
        potentiationResource -= CONSUMPTION_RESOURCE;
        return true;
    }

    private void restoreResource() {
        if (potentiationResource >= RESOURCE_FULL || lastTransfer == null) return;
        long time = Duration.between(lastTransfer, Instant.now()).toMillis();
        potentiationResource += time;
        if (potentiationResource > RESOURCE_FULL) potentiationResource = RESOURCE_FULL;
    }


    public ArrayList<Destination> createTransmitters(Spike spike) {
        restoreResource();
        ArrayList<Destination> destinations = null;
        boolean sent = false;
        if (consumptionResource()) {
            if (destination != null) {
                sent = true;
                destinations = new ArrayList<>();
                Destination newDestination = destination.copy();
                newDestination.setTransmittersFromSender(transmitters);
                destinations.add(newDestination);
            }
//            else {
//                System.out.println("Empty dest for " + toString());
//            }
        }
        if (sent) {
            activated.setValueInstantly(true);
            lastTransfer = Instant.now();
        } else {
            calculateWishes(true);
        }
        return destinations;
    }

    private void calculateWishes(boolean increment) {
        if (increment) {
            if (destination == null) {
                wishGetReceiver += Config.wishGetReceiverIncr;
            } else {
                wishGetReceiver = 0;
            }
        }
    }

    public Set<Synapse.Type> transmittersTypes() {
        Set<Synapse.Type> types = new HashSet<>();
        for (Transmitter transmitter : this.getTransmitters()) {
            types.add(transmitter.getSynapse().getType());
        }
        return types;
    }

    public synchronized void setReceiverCandidate(Receiver receiver) {
        receiverCandidates.merge(receiver, 1, (a, b) -> a + b);

        if (receiverCandidates.size() > 10) {
            Receiver bestReceiver = null;
            int bestCount = 0;
            for (Receiver r : receiverCandidates.keySet()) {
                if (receiverCandidates.remove(r) > bestCount) {
                    bestReceiver = r;
                }
            }
            setReceiver(bestReceiver);
            receiverCandidates.clear();
        }

    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;

        if (receiver != null) {
            receiver_id = receiver.getId();
            rlayer_id = receiver.getDendrite().getNeuron().getLayerId();
            rdendrite_id = receiver.getDendrite().getId();
            rneuron_id = receiver.getDendrite().getNeuron().getId();

            receiver.setSender(this);
            destination = new Destination();
            destination.setLayerId(receiver.getDendrite().getNeuron().getLayerId());
            destination.setNeuronId(receiver.getDendrite().getNeuron().getId());
            destination.setDendriteId(receiver.getDendrite().getId());
            destination.setReceiverId(receiver.getId());
            destination.setTransmittersFromSender(this.transmitters);
        }
    }

    public void delete() {
        axon.getSenders().remove(this);
        axon.getNeuron().getDeleted().add(getName());
        transmitters.clear();
        axon = null;
        receiver.setSender(null);
        clearDestination();
    }

    public void clearDestination() {
        this.receiver = null;
        this.destination = null;
//        this.receiver_id = null;
//        this.rlayer_id = null;
//        this.rdendrite_id = null;
//        this.rneuron_id = null;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public Destination getDestination() {
        return destination;
    }

    public void addTransmitters(Transmitter transmitter) {
        transmitters.add(transmitter);
    }

    public Axon getAxon() {
        return axon;
    }

    public String getName() {
        if (name == null) name = axon.getName() + "S" + id;
        return name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Transmitter> getTransmitters() {
        return transmitters;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public long getWishGetReceiver() {
        return wishGetReceiver;
    }

    public void setWishGetReceiver(long wishGetReceiver) {
        this.wishGetReceiver = wishGetReceiver;
    }

    public Long getLayer_id() {
        return layer_id;
    }

    public void setLayer_id(Long layer_id) {
        this.layer_id = layer_id;
    }

    public Long getAxon_id() {
        return axon_id;
    }

    public void setAxon_id(Long axon_id) {
        this.axon_id = axon_id;
    }

    public Long getNeuron_id() {
        return neuron_id;
    }

    public void setNeuron_id(Long neuron_id) {
        this.neuron_id = neuron_id;
    }

    public Long getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(Long receiver_id) {
        this.receiver_id = receiver_id;
    }

    public Long getRlayer_id() {
        return rlayer_id;
    }

    public void setRlayer_id(Long rlayer_id) {
        this.rlayer_id = rlayer_id;
    }

    public Long getRdendrite_id() {
        return rdendrite_id;
    }

    public void setRdendrite_id(Long rdendrite_id) {
        this.rdendrite_id = rdendrite_id;
    }

    public Long getRneuron_id() {
        return rneuron_id;
    }

    public void setRneuron_id(Long rneuron_id) {
        this.rneuron_id = rneuron_id;
    }

    public LastValueHolder<Boolean> getActivated() {
        return activated;
    }

    public long getPotentiationResource() {
        return potentiationResource;
    }

    public long getPotentiationBalance() {
        return potentiationBalance;
    }

    @Override
    public String toString() {
        return "Sender{" +
                (name != null ? "name='" + name + '\'' + "," : "") +
                '}';
    }
}
