package pro.belbix.bbrain.neuron;

import pro.belbix.bbrain.models.Id;
import pro.belbix.bbrain.models.Key;
import pro.belbix.bbrain.models.ModelVariable;
import pro.belbix.bbrain.models.SimpleKey;
import pro.belbix.bbrain.utils.Config;
import pro.belbix.bbrain.visual.LastValueHolder;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Created by v.belykh on 25.04.2018
 * Класс нейрона по сути выступает просто в роли обертки для аксонов и дендритов
 * А так же их управляющих методов
 */
public class Neuron {

    public Neuron() {
    }

    // --------------- STATE FIELDS -----------------------
    @Id
    private long id = 0;
    @Id
    private Long layer_id;
    @ModelVariable
    private String name = null;
    @ModelVariable
    private Long membraneResistance = Config.membraneResistanceDefault;
    @ModelVariable
    private Instant lastSpike = null;
    @ModelVariable
    private long amountPotentials = 0;
    @ModelVariable
    private Long refractoryResist = null;
    @ModelVariable
    private LastValueHolder<String> inputInfo = new LastValueHolder<>(Config.visualisationPause);
    @ModelVariable
    private float x = 0;
    @ModelVariable
    private float y = 0;
    @ModelVariable
    private float z = 0;

    //------------- PARENT ----------------------------------
    private Layer layer = null;

    //------------- CHILD -----------------------------------
    private List<Dendrite> dendrits = new CopyOnWriteArrayList<>();
    private List<Axon> axons = new CopyOnWriteArrayList<>();

    //------------- FOR PROCESSING --------------------------
    private List<ElectricalPotential> potentials = new CopyOnWriteArrayList<>();

    //------------- LOCAL VARIABLE --------------------------
    private SimpleKey key = null;
    private long epCount = 0;
    private Instant lTime = null;
    private long wishDendrite = 0;
    public volatile boolean activated = false;
    private LastValueHolder<Long> lastAmountPotentials = new LastValueHolder<>(Config.visualisationPause);
    private Instant lastVisualization = null;
    private boolean absoluteRefractory = false;
    private Instant currentTime = null;
    private double activity = 0;
    private CopyOnWriteArraySet<Instant> activities = new CopyOnWriteArraySet<>();
    private ConcurrentLinkedQueue<String> deleted = new ConcurrentLinkedQueue<>();
    public final Object lock = new Object();
    private long dendriteIdCount = 0;
    private boolean readyToShow = false;

    public synchronized boolean processing() {
        if (layer.getProcessingTime() == null) {
            lTime = Instant.now();
        } else {
            lTime = layer.getProcessingTime();
        }
        boolean sent = checkCharge();

//        if(!layer.isImmutable()) { //TODO полная херь
//            if(hasDestination()) modifyWishes(sent);
//        }
        calculateDeaths();
        return sent;
    }

    public void addDendrite(Dendrite dendrite) {
        dendrits.add(dendrite);
    }

    public void addAxon(Axon axon) {
        axons.add(axon);
    }

    /**
     * Отсюда должен начинаться пиковый ПОТЕНЦИАЛ ДЕЙСТВИЯ(spike) проходящий по аксонному каналу
     */
    public void createSpikeInAxon() {
        setLastSpike(lTime);
        activated = true;
        activities.add(Instant.now());
        for (Axon axon : axons) {
            axon.setActivateSpike(true);
            axon.setActivationTime(lTime);
        }
    }

    private boolean checkCharge() {
        setAmountPotentials(0);
        for (ElectricalPotential potential : potentials) {
            long lifeTime = Duration.between(potential.getInitTime(), lTime).toMillis();
            if (lifeTime > Config.epLifeTimeMax) {
                potentials.remove(potential);
                continue;
            }

            setAmountPotentials(amountPotentials + epPower(potential.getPotential(), lifeTime));
            lastAmountPotentials.setValueInstantly(amountPotentials);
        }
        long refractory = checkRefractoryResistance(lastSpike, lTime);
        if (refractory == -1L) { //absolute refractory
            absoluteRefractory = true;
            return false;
        } else {
            absoluteRefractory = false;
        }
        setRefractoryResist(refractory);

        if (getAmountPotentials() >= (getMembraneResistance() + refractoryResist)) {
            createSpikeInAxon();
            lastAmountPotentials.setValueInstantly(amountPotentials);
            setAmountPotentials(0);
            potentials.clear();
            return true;
        }
        return false;
    }

    private void calculateWishes(boolean increment) {
        if (increment) {
            if (dendrits.size() < Config.maxDendrites) {
                wishDendrite += Config.wishDendriteIncr;
            }
        } else {
            wishDendrite -= Config.wishDendriteIncr;
        }
    }

    private void calculateDeaths(){
        for(Dendrite dendrite: dendrits){
            dendrite.calculateDeath();
        }
    }

    private synchronized void calculateActivities() {
        this.activity = Neuron.calculateActivities(activities, Instant.now(), layer.getActivityMax());
    }

    public double getActivity() {
        calculateActivities();
        return activity;
    }

    public long getMembraneResistance() {
        return membraneResistance;
    }


    private static long checkRefractoryResistance(Instant lastSpike, Instant now) {
        if (lastSpike == null) return 0L;
        float refractoryPeriod = Duration.between(lastSpike, now).toMillis();
        float ep = Duration.of(Config.epLifeTimeMax, ChronoUnit.MILLIS).toMillis();
        float def = refractoryPeriod / ep;
        if (def > 1) return 0L;
        if (def < Config.refractoryPeriodAbsolute) return -1L;
        return (long) ((float) Config.refractoryPeriodMax * def);
    }

    private static long epPower(long potential, long time) {
        return potential * (1 - (time / Config.epLifeTimeMax)); //TODO formula
    }

    public ArrayList<Receiver> getDendriteReceivers() {
        ArrayList<Receiver> receivers = new ArrayList<>();
        for (Dendrite dendrite : getDendrits()) {
            receivers.addAll(dendrite.getReceivers());
        }
        return receivers;
    }

    public Key getKey() {
        if (key == null) {
            key = new SimpleKey();
            key.setLong1(getId());
            key.setLong2(getLayer_id());
        }
        return key;
    }

    public String getName() {
        if (name == null) name = "L" + getLayerId() + "N" + id;
        return name;
    }

    public void setLayer(Layer layer) {
        this.layer = layer;
        layer_id = layer.getId();
    }

    public void modifyWishes(boolean sent) {
//        calculateWishes(sent);
        for (Dendrite dendrite : dendrits) {
//            if(dendrite.getActivity() > layer.getActivationMinForModify()) {
            dendrite.calculateWishes(sent);
//            }
//            for (Receiver receiver : dendrite.getReceivers()) {
////                if(receiver.getActivity() > layer.getActivationMinForModify()) {
//                    receiver.calculateWishes(sent);
////                }
//            }
        }
    }

    public List<Receiver> deleteDendrite() {
        double bestActivity = -1;
        Dendrite bestDendrite = null;
        for (Dendrite dendrite : dendrits) {
            double activity = dendrite.getActivity();
            if (activity > bestActivity) {
                bestActivity = activity;
                bestDendrite = dendrite;
            }
        }
        if (bestDendrite == null) return null;
        dendrits.remove(bestDendrite);
        return bestDendrite.delete();
    }

    public boolean modify(Transmitter transmitter) {
        boolean stimulation;
        switch (transmitter.getSynapse().getType()) {
            case STIMULATION:
                stimulation = true;
                break;
            case PUNISH:
                stimulation = false;
                break;
            default:
                return false;
        }

        //receptor energy
        for (Receiver receiver : getDendriteReceivers()) {
            double k = 10 * (receiver.getActivity() / layer.getActivityMax());
            for (Receptor receptor : receiver.getReceptors()) {
                if (stimulation) {
                    receptor.increaseEnergy(k);
                } else {
                    receptor.decreaseEnergy(k);
                }
            }
        }

        //transmitter
//        for(Axon axon: axons){
//            for(Sender sender: axon.getSenders()){
//                for(Transmitter transmitter1: sender.getTransmitters()){
//
//                }
//            }
//        }

        //TODO resist
        return true;
    }

    public static double calculateActivities(Set<Instant> activities, Instant currentTime, double lifeTimeMax) {
        if (activities.isEmpty()) return 0;
        if (currentTime == null) currentTime = Instant.now();
        double activity = 0;
        for (Instant instant : activities) {
            double lifeTme = Duration.between(instant, currentTime).toMillis();
            if (lifeTme > lifeTimeMax) {
                activities.remove(instant);
                continue;
            }
            if (lifeTme <= 0) continue;
            activity += lifeTimeMax - lifeTme;
        }
        return activity;
    }

    private boolean hasDestination() {
        for (Axon axon : axons) {
            for (Sender sender : axon.getSenders()) {
                if (sender.getDestination() != null) return true;
            }
        }
        return false;
    }

    public void setAmountPotentials(long amountPotentials) {
        this.amountPotentials = amountPotentials;
    }

    public synchronized long nextEpCount() {
        return epCount++;
    }

    public List<Dendrite> getDendrits() {
        return dendrits;
    }

    public List<Axon> getAxons() {
        return axons;
    }


    public long getLayerId() {
        return layer.getId();
    }

    public List<ElectricalPotential> getPotentials() {
        return potentials;
    }

    public Instant getLastSpike() {
        return lastSpike;
    }

    public void setLastSpike(Instant lastSpike) {
        this.lastSpike = lastSpike;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Layer getLayer() {
        return layer;
    }


    public long getAmountPotentials() {
        return amountPotentials;
    }


    public Long getRefractoryResist() {
        return refractoryResist;
    }

    public void setRefractoryResist(Long refractoryResist) {
        this.refractoryResist = refractoryResist;
    }

    public long getWishDendrite() {
        return wishDendrite;
    }

    public void setWishDendrite(long wishDendrite) {
        this.wishDendrite = wishDendrite;
    }

    public Long getLayer_id() {
        return layer_id;
    }

    public void setLayer_id(Long layer_id) {
        this.layer_id = layer_id;
    }

    public LastValueHolder<String> getInputInfo() {
        return inputInfo;
    }

    public LastValueHolder<Long> getLastAmountPotentials() {
        return lastAmountPotentials;
    }

    public Instant getLastVisualization() {
        return lastVisualization;
    }

    public void setLastVisualization(Instant lastVisualization) {
        this.lastVisualization = lastVisualization;
    }

    public boolean isAbsoluteRefractory() {
        return absoluteRefractory;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public ConcurrentLinkedQueue<String> getDeleted() {
        return deleted;
    }

    public long nextDendriteId() {
        return dendriteIdCount++;
    }

    public boolean isReadyToShow() {
        return readyToShow;
    }

    public void setReadyToShow(boolean readyToShow) {
        this.readyToShow = readyToShow;
    }

    @Override
    public String toString() {
        return "Neuron{" +
                (getName() != null ? "name='" + getName() + '\'' + "," : "") +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Neuron)) return false;

        Neuron neuron = (Neuron) o;

        if (getId() != neuron.getId()) return false;
        return getLayer_id() != null ? getLayer_id().equals(neuron.getLayer_id()) : neuron.getLayer_id() == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getLayer_id() != null ? getLayer_id().hashCode() : 0);
        return result;
    }
}
