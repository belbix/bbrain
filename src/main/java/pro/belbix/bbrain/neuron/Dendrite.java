package pro.belbix.bbrain.neuron;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.models.Id;
import pro.belbix.bbrain.models.ModelVariable;
import pro.belbix.bbrain.utils.Config;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Created by v.belykh on 25.04.2018
 */
public class Dendrite {
    private static final Logger log = LogManager.getRootLogger();

    // --------------- STATE FIELDS -----------------------
    @Id
    private long id = 0;
    @Id
    private Long layer_id;
    @Id
    private Long neuron_id;
    @ModelVariable
    private String name = null;


    //------------- PARENT ----------------------------------
    private Neuron neuron = null;

    //------------- CHILD -----------------------------------
    private List<Receiver> receivers = new CopyOnWriteArrayList<>();

    //------------- LOCAL VARIABLE --------------------------
    private long wishReceiver = 0;
    private Instant currentTime = null;
    private double activity = 0;
    private CopyOnWriteArraySet<Instant> activities = new CopyOnWriteArraySet<>();
    private boolean deleted = false;
    private long receiverIdCoint = 0;

    public Dendrite(Neuron neuron) {
        this.neuron = neuron;
        layer_id = neuron.getLayerId();
        neuron_id = neuron.getId();
        neuron.addDendrite(this);
    }

    public void addReceiver(Receiver receiver) {
        receivers.add(receiver);
    }

    public void addPotential(ElectricalPotential potential) {
        log.trace(getName() + " potential added " + potential);

        if (neuron.getLayer().getProcessingTime() == null) {
            currentTime = Instant.now();
        } else {
            currentTime = neuron.getLayer().getProcessingTime();
        }
        calculateActivities();

        //TODO дендрит тоже может сгенерить спайк, надо чекать в будущем
        //TODO выполнение промежуточной суммации
        neuron.getPotentials().add(potential);
        neuron.setReadyToShow(true);
        activities.add(Instant.now());
    }

    public void calculateWishes(boolean increment) {
        double k = getActivity() / neuron.getLayer().getActivityMax();
        if (increment) {
            if (receivers.size() < Config.maxReceivers) {
                wishReceiver += ((double)Config.wishCreateReceiverIncr) * k;
            }
        } else {
            wishReceiver -= ((double)Config.wishCreateReceiverIncr) * k;
        }
    }

    public void calculateDeath(){
        for(Receiver receiver: receivers){
            receiver.calculateDeath();
        }
    }

    public Receiver deleteReceiver() {
        double bestActivity = -1;
        Receiver bestReceiver = null;
        for (Receiver receiver : receivers) {
            double activity = receiver.getActivity();
            if (activity > bestActivity) {
                bestActivity = activity;
                bestReceiver = receiver;
            }
        }
        if (bestReceiver == null) return null;
        bestReceiver.delete();
        receivers.remove(bestReceiver);
        return bestReceiver;
    }

    public List<Receiver> delete() {
        for (Receiver receiver : receivers) {
            receiver.delete();
        }
        receivers.clear();

        neuron.getDendrits().remove(this);
        neuron.getDeleted().add(getName());
        neuron = null;
        deleted = true;
        return receivers;
    }

    private synchronized void calculateActivities() {
        this.activity = Neuron.calculateActivities(activities, Instant.now(), neuron.getLayer().getActivityMax());
    }

    public double getActivity() {
        calculateActivities();
        return activity;
    }

    public List<Receiver> getReceivers() {
        return receivers;
    }

    public String getName() {
        if (name == null) name = neuron.getName() + "D" + id;
        return name;
    }

    public long nextReceiverId(){
        return receiverIdCoint++;
    }


    public Neuron getNeuron() {
        return neuron;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getWishReceiver() {
        return wishReceiver;
    }

    public void setWishReceiver(long wishReceiver) {
        this.wishReceiver = wishReceiver;
    }

    public Long getLayer_id() {
        return layer_id;
    }

    public void setLayer_id(Long layer_id) {
        this.layer_id = layer_id;
    }

    public Long getNeuron_id() {
        return neuron_id;
    }

    public void setNeuron_id(Long neuron_id) {
        this.neuron_id = neuron_id;
    }

    @Override
    public String toString() {
        return "Dendrite{" +
                (name != null ? "name='" + name + '\'' + "," : "") +
                '}' + deleted;
    }


}