package pro.belbix.bbrain.neuron;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.utils.Config;

import java.util.HashMap;

/**
 * Created by v.belykh on 11.05.2018
 */
public class Photoreceptor {
    private static final Logger log = LogManager.getRootLogger();
    private long id = 0;
    private long layerId = 0;
    private Receiver receiver = null;
    private int transmitterCount = Config.photoreceptorTransmitterCount;

    public void receiveColor(int color){
        if (receiver == null) return;
        receiver.receiveTransmitter(createTransmitter(color));
    }

    private Transmitter createTransmitter(int color){
        Transmitter transmitter = new Transmitter();
        transmitter.setSynapse(new Synapse(Synapse.Type.ACTIVATOR));
        transmitter.setCount(calcTransmittersCountByColor(color));
        return transmitter;
    }

    private int calcTransmittersCountByColor(int color){
        float k = (float)transmitterCount / (float)color;
        int count = (int)((float)transmitterCount * k);
//        log.debug("color: " + color + " count: " + count + " transmitterCount: " + transmitterCount + " k: " + k);
        return count;
    }

    public static HashMap<Integer, Photoreceptor> createPhotoreceptors(int count){
        HashMap<Integer, Photoreceptor> photoreceptors = new HashMap<>();
        log.info("Start create photoreceptors");
        int i = 0;
        while (i < count){
            Photoreceptor p = new Photoreceptor();
            photoreceptors.put(i, p);
            i++;
        }
        log.info("Create " + photoreceptors.size() + " photoreceptors");
        return photoreceptors;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLayerId() {
        return layerId;
    }

    public void setLayerId(long layerId) {
        this.layerId = layerId;
    }
}
