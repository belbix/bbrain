package pro.belbix.bbrain.neuron;

/**
 * Created by v.belykh on 11.05.2018
 */
public class Spike {
    private String name = "";
    private long initTime = System.nanoTime();
    private Axon fromAxon = null;

    public long getInitTime() {
        return initTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Axon getFromAxon() {
        return fromAxon;
    }

    public void setFromAxon(Axon fromAxon) {
        this.fromAxon = fromAxon;
    }
}
