package pro.belbix.bbrain.neuron;

import pro.belbix.bbrain.models.Id;
import pro.belbix.bbrain.utils.Config;

/**
 * Created by v.belykh on 15.05.2018
 */
public class Transmitter {
    // --------------- STATE FIELDS -----------------------
    @Id
    private long id = 0;
    @Id
    private Long layer_id;
    @Id
    private Long sender_id;
    @Id
    private Long axon_id;
    @Id
    private Long neuron_id;
    private String name = null;
    private Synapse synapse = null;
    private int count = 0;
    private int modifyReceptorPowerValue = 0;
    private int needToActivation = Config.transmitterDefaultNeedActivation;

    //------------- PARENT ----------------------------------
    private Sender sender = null;

    public void setSender(Sender sender) {
        this.sender = sender;
        layer_id = sender.getAxon().getNeuron().getLayerId();
        sender_id = sender.getId();
        axon_id = sender.getAxon().getId();
        neuron_id = sender.getAxon().getNeuron().getId();
    }

    public int needToActivation() {
//        switch (type){
//            case ACTIVATOR:
//                return 25;
//        }
//        throw new RuntimeException("Unsupport mediator!");
        return needToActivation;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getModifyReceptorPowerValue() {
        return modifyReceptorPowerValue;
    }

    public void setModifyReceptorPowerValue(int modifyReceptorPowerValue) {
        this.modifyReceptorPowerValue = modifyReceptorPowerValue;
    }

    public int getNeedToActivation() {
        return needToActivation;
    }

    public void setNeedToActivation(int needToActivation) {
        this.needToActivation = needToActivation;
    }

    public String getName() {
        if (name == null) name = sender.getName() + "T" + id;
        return name;
    }

    public Sender getSender() {
        return sender;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Synapse getSynapse() {
        return synapse;
    }

    public void setSynapse(Synapse synapse) {
        this.synapse = synapse;
    }

    public Long getLayer_id() {
        return layer_id;
    }

    public void setLayer_id(Long layer_id) {
        this.layer_id = layer_id;
    }

    public Long getSender_id() {
        return sender_id;
    }

    public void setSender_id(Long sender_id) {
        this.sender_id = sender_id;
    }

    public Long getAxon_id() {
        return axon_id;
    }

    public void setAxon_id(Long axon_id) {
        this.axon_id = axon_id;
    }

    public Long getNeuron_id() {
        return neuron_id;
    }

    public void setNeuron_id(Long neuron_id) {
        this.neuron_id = neuron_id;
    }

    @Override
    public String toString() {
        return "Transmitter{" +
                "id='" + id + '\'' + "," +
                (getName() != null ? "name='" + getName() + '\'' + "," : "") +
                (synapse != null ? "synapse=" + synapse + "," : "") +
                (sender != null ? "sender=" + sender + "," : "") +
                "count='" + count + '\'' + "," +
                "modifyReceptorPowerValue='" + modifyReceptorPowerValue + '\'' + "," +
                "needToActivation='" + needToActivation + '\'' + "," +
                '}';
    }
}
