package pro.belbix.bbrain.neuron;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity for external Receiver
 * Created by v.belykh on 9/27/2018
 */
public class Destination {
    private Long layerId;
    private Long neuronId;
    private Long dendriteId;
    private Long receiverId;
    private List<Transmitter> transmitters;

    @Override
    public String toString() {
        return "Destination{" +
                (layerId != null ? "layerId=" + layerId + "," : "") +
                (neuronId != null ? "neuronId=" + neuronId + "," : "") +
                (dendriteId != null ? "dendriteId=" + dendriteId + "," : "") +
                (receiverId != null ? "receiverId=" + receiverId + "," : "") +
                (transmitters != null ? "transmitters=" + transmitters + "," : "") +
                '}';
    }

    public Destination copy(){
        Destination destination = new Destination();
        destination.setLayerId(layerId);
        destination.setNeuronId(neuronId);
        destination.setDendriteId(dendriteId);
        destination.setReceiverId(receiverId);
        destination.setTransmitters(transmitters);
        return destination;
    }

    public void setTransmittersFromSender(List<Transmitter> transmitters){
        ArrayList<Transmitter> t = new ArrayList<>();
        t.addAll(transmitters);
        setTransmitters(t);
    }

    public void setSingleTransmitter(Transmitter transmitter){
        ArrayList<Transmitter> t = new ArrayList<>();
        t.add(transmitter);
        setTransmitters(t);
    }

    public Long getLayerId() {
        return layerId;
    }

    public void setLayerId(Long layerId) {
        this.layerId = layerId;
    }

    public Long getNeuronId() {
        return neuronId;
    }

    public void setNeuronId(Long neuronId) {
        this.neuronId = neuronId;
    }

    public Long getDendriteId() {
        return dendriteId;
    }

    public void setDendriteId(Long dendriteId) {
        this.dendriteId = dendriteId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public List<Transmitter> getTransmitters() {
        return transmitters;
    }

    public void setTransmitters(List<Transmitter> transmitters) {
        this.transmitters = transmitters;
    }
}
