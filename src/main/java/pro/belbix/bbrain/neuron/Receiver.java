package pro.belbix.bbrain.neuron;

import pro.belbix.bbrain.models.Id;
import pro.belbix.bbrain.models.Key;
import pro.belbix.bbrain.models.ModelVariable;
import pro.belbix.bbrain.models.SimpleKey;
import pro.belbix.bbrain.processing.ProcessingCenter;
import pro.belbix.bbrain.utils.Config;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Created by v.belykh on 25.04.2018
 */
public class Receiver {
    // --------------- STATE FIELDS -----------------------
    @Id
    private long id = 0;
    @Id
    private Long layer_id;
    @Id
    private Long dendrite_id;
    @Id
    private Long neuron_id;
    @ModelVariable
    private String name = null;

    //------------- PARENT ----------------------------------
    private Dendrite dendrite = null;

    //------------- CHILD -----------------------------------
    private CopyOnWriteArrayList<Receptor> receptors = new CopyOnWriteArrayList<>();

    //------------- PREVIOUS NEURON -------------------------
    private Sender sender = null;

    //------------- FOR PROCESSING --------------------------
    private ConcurrentLinkedQueue<Transmitter> transmitters = new ConcurrentLinkedQueue<>();

    //------------- LOCAL VARIABLE --------------------------
    public static final long MAX_LAST_PROCESSING = 10_000;
    private Instant lastActivity = null;
    private SimpleKey key = null;
    private double activity = 0;
    private CopyOnWriteArraySet<Instant> activities = new CopyOnWriteArraySet<>();
    private long wishSender = 0;
    private long death = 0;
    private boolean deleted = false;

    //------------- PROCESSING STATE-------------------------
    private Instant currentTime = null;

    public Receiver(Dendrite dendrite) {
        setDendrite(dendrite);
    }

    public synchronized void processing() {
        if (dendrite.getNeuron().getLayer().getProcessingTime() == null) {
            currentTime = Instant.now();
        } else {
            currentTime = dendrite.getNeuron().getLayer().getProcessingTime();
        }
        Transmitter transmitter;
        while (true) {
            calculateActivities();
            transmitter = transmitters.poll();
            if (transmitter == null) break;
            receiveTransmitter(transmitter);
        }
    }

    private synchronized void calculateActivities() {
        this.activity = Neuron.calculateActivities(activities, Instant.now(), dendrite.getNeuron().getLayer().getActivityMax());
    }

    public double getActivity() {
        calculateActivities();
        return activity;
    }

    public void calculateWishes(boolean increment){
        double k = getActivity() / dendrite.getNeuron().getLayer().getActivityMax();
        if(increment) {
            wishSender += ((double)Config.wishCreateReceiverIncr) * k;
        } else {
            wishSender -= ((double)Config.wishCreateReceiverIncr) * k;
        }
    }

    public void calculateDeath(){
        if(lastActivity == null){
            death++;
            return;
        }
        if(Duration.between(lastActivity, Instant.now()).toMillis() > Config.epLifeTimeMax * 10){
            lastActivity = null;
            death++;
        } else {
            death = 0;
        }
    }

    public void receiveTransmitter(Transmitter transmitter) {
        for (Receptor receptor : receptors) {
            boolean activate;
            if (receptor.activation(transmitter)) {
                activate = dendrite.getNeuron().modify(transmitter);
                if (receptor.getEnergy() != 0) {
                    ElectricalPotential ep = new ElectricalPotential(receptor, transmitter);
                    ep.setInitTime(currentTime);
                    dendrite.addPotential(ep);
                    activate = true;
                }
                if(activate) {
                    lastActivity = Instant.now();
                    activities.add(lastActivity);
                }
            }
        }
    }

    public void addReceptor(Receptor receptor) {
        receptors.add(receptor);
    }

    public Key getKey() {
        if (key == null) {
            key = new SimpleKey();
            key.setLong1(getId());
            key.setLong2(getLayer_id());
            key.setLong3(getDendrite_id());
            key.setLong4(getNeuron_id());
        }
        return key;
    }

    public String getName() {
        if (name == null) name = createName(dendrite.getNeuron().getLayerId(),
                dendrite.getNeuron().getId(), dendrite.getId(), id);
        return name;
    }

    public static String createName(long layerId, long neuronId, long dendriteId, long receiverId) {
        return "L" + layerId + "N" + neuronId + "D" + dendriteId + "RV" + receiverId;
    }

    private void setDendrite(Dendrite dendrite) {
        this.dendrite = dendrite;
        layer_id = dendrite.getNeuron().getLayerId();
        dendrite_id = dendrite.getId();
        neuron_id = dendrite.getNeuron().getId();
    }

    public CopyOnWriteArrayList<Receptor> getReceptors() {
        return receptors;
    }


    public void addTransmitter(Transmitter transmitter) {
        if (transmitter == null) return;
        transmitters.add(transmitter);
    }

    public void delete(){
        for(Receptor receptor: receptors){
            dendrite.getNeuron().getDeleted().add(receptor.getName());
            receptor.delete();
        }
        receptors.clear();

        dendrite.getNeuron().getDeleted().add(getName());
        dendrite.getReceivers().remove(this);

        deleteSender();

        dendrite = null;
        deleted = true;
    }

    public void deleteSender(){
        if(sender != null) {
            sender.delete();
            sender = null;
        }
    }

    public Dendrite getDendrite() {
        return dendrite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setActivity(double activity) {
        this.activity = activity;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Long getLayer_id() {
        return layer_id;
    }

    public void setLayer_id(Long layer_id) {
        this.layer_id = layer_id;
    }

    public Long getDendrite_id() {
        return dendrite_id;
    }

    public void setDendrite_id(Long dendrite_id) {
        this.dendrite_id = dendrite_id;
    }

    public Long getNeuron_id() {
        return neuron_id;
    }

    public void setNeuron_id(Long neuron_id) {
        this.neuron_id = neuron_id;
    }

    public ConcurrentLinkedQueue<Transmitter> getTransmitters() {
        return transmitters;
    }

    public long getWishSender() {
        return wishSender;
    }

    public void setWishSender(long wishSender) {
        this.wishSender = wishSender;
    }

    public long getDeath() {
        return death;
    }

    public void setDeath(long death) {
        this.death = death;
    }

    public Instant getLastActivity() {
        return lastActivity;
    }

    @Override
    public String toString() {
        return "Receiver{" +
                (name != null ? "name='" + name + '\'' + "," : "") +
                '}'  + deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Receiver)) return false;

        Receiver receiver = (Receiver) o;

        return getName() != null ? getName().equals(receiver.getName()) : receiver.getName() == null;
    }

    @Override
    public int hashCode() {
        return getName() != null ? getName().hashCode() : 0;
    }
}