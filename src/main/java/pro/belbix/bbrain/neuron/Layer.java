package pro.belbix.bbrain.neuron;

import pro.belbix.bbrain.models.Id;
import pro.belbix.bbrain.utils.Config;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by v.belykh on 23.07.2018
 */
public class Layer {
    // --------------- STATE FIELDS -----------------------
    @Id
    private long id = 0;
    private long y = 0;
    private String name = null;
    private LayerType layerType;
    private Instant processingTime = null;
    private long neuronCount = 0;

    private long dendriteCount = 1;
    private long receiverCount = 1;

    private long axonCount = 1;
    private long senderCount = 1;
    private int senderMax = 1;

    private long receptorEnergy = Config.receiverReceptorDefaultEnergy;
    private long transmitterCount = Config.transmitterDefaultCount;
    private long transmitterNeedActivation = Config.transmitterDefaultNeedActivation;
    private long transmitterModifyReceptorPower = 0;
    private long deathMax = 100;
    private long activationMinForModify = Config.epLifeTimeMax / 2;
    private double activityMax = Config.epLifeTimeMax * 1.5;
    private boolean immutable = false;
    private final List<Synapse.Type> dendriteSynapses = new ArrayList<>();
    private final List<Synapse.Type> axonSynapses = new ArrayList<>();

    //------------- CHILD -----------------------------------
    private Map<Long, Neuron> neurons = new HashMap<>();
    private ArrayList<Synapse> receiverSynapse = new ArrayList<>();
    private ArrayList<Synapse> senderSynapse = new ArrayList<>();

    //------------- PROPERTY -----------------------------------
    private long neuronResist = Config.membraneResistanceDefault;
    private long axonTimeNeedToActivation = Config.axonDefaultTimeNeedToActivation;

    public enum LayerType {
        RECEPTOR, ASSOCIATIVE, PROCESSING, MEMORY, MANIPULATOR, PLEASURE, PUNISHMENT
    }

    public long getNeuronResist() {
        return neuronResist;
    }

    public void setNeuronResist(long neuronResist) {
        this.neuronResist = neuronResist;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(Instant processingTime) {
        this.processingTime = processingTime;
    }

    public Map<Long, Neuron> getNeurons() {
        return neurons;
    }

    public void setNeurons(HashMap<Long, Neuron> neurons) {
        this.neurons = neurons;
    }

    public long getNeuronCount() {
        return neuronCount;
    }

    public void setNeuronCount(long neuronCount) {
        this.neuronCount = neuronCount;
    }

    public long getDendriteCount() {
        return dendriteCount;
    }

    public void setDendriteCount(long dendriteCount) {
        this.dendriteCount = dendriteCount;
    }

    public ArrayList<Synapse> getReceiverSynapse() {
        return receiverSynapse;
    }

    public void setReceiverSynapse(ArrayList<Synapse> receiverSynapse) {
        receiverSynapse.forEach(x -> x.setLayerId(id));
        this.receiverSynapse = receiverSynapse;
    }

    public ArrayList<Synapse> getSenderSynapse() {
        return senderSynapse;
    }

    public void setSenderSynapse(ArrayList<Synapse> senderSynapse) {
        this.senderSynapse = senderSynapse;
    }

    public long getAxonCount() {
        return axonCount;
    }

    public void setAxonCount(long axonCount) {
        this.axonCount = axonCount;
    }

    public long getReceptorEnergy() {
        return receptorEnergy;
    }

    public void setReceptorEnergy(long receptorEnergy) {
        this.receptorEnergy = receptorEnergy;
    }

    public long getTransmitterCount() {
        return transmitterCount;
    }

    public void setTransmitterCount(long transmitterCount) {
        this.transmitterCount = transmitterCount;
    }

    public long getTransmitterNeedActivation() {
        return transmitterNeedActivation;
    }

    public void setTransmitterNeedActivation(long transmitterNeedActivation) {
        this.transmitterNeedActivation = transmitterNeedActivation;
    }

    public long getTransmitterModifyReceptorPower() {
        return transmitterModifyReceptorPower;
    }

    public void setTransmitterModifyReceptorPower(long transmitterModifyReceptorPower) {
        this.transmitterModifyReceptorPower = transmitterModifyReceptorPower;
    }

    public long getAxonTimeNeedToActivation() {
        return axonTimeNeedToActivation;
    }

    public void setAxonTimeNeedToActivation(long axonTimeNeedToActivation) {
        this.axonTimeNeedToActivation = axonTimeNeedToActivation;
    }

    public LayerType getLayerType() {
        return layerType;
    }

    public void setLayerType(LayerType layerType) {
        this.layerType = layerType;
    }

    public long getReceiverCount() {
        return receiverCount;
    }

    public void setReceiverCount(long receiverCount) {
        this.receiverCount = receiverCount;
    }

    public long getSenderCount() {
        return senderCount;
    }

    public void setSenderCount(long senderCount) {
        this.senderCount = senderCount;
    }

    public int getSenderMax() {
        return senderMax;
    }

    public void setSenderMax(int senderMax) {
        this.senderMax = senderMax;
    }

    public long getY() {
        return y;
    }

    public void setY(long y) {
        this.y = y;
    }

    public long getActivationMinForModify() {
        return activationMinForModify;
    }

    public void setActivationMinForModify(long activationMinForModify) {
        this.activationMinForModify = activationMinForModify;
    }

    public boolean isImmutable() {
        return immutable;
    }

    public void setImmutable(boolean immutable) {
        this.immutable = immutable;
    }

    public List<Synapse.Type> getDendriteSynapses() {
        return dendriteSynapses;
    }

    public List<Synapse.Type> getAxonSynapses() {
        return axonSynapses;
    }

    public double getActivityMax() {
        return activityMax;
    }

    public void setActivityMax(double activityMax) {
        this.activityMax = activityMax;
    }

    public long getDeathMax() {
        return deathMax;
    }

    public void setDeathMax(long deathMax) {
        this.deathMax = deathMax;
    }

    @Override
    public String toString() {
        return "Layer{" +
                "id='" + id + '\'' + "," +
                (name != null ? "name='" + name + '\'' + "," : "") +
                (processingTime != null ? "processingTime=" + processingTime + "," : "") +
                (neurons != null ? "neurons=" + neurons + "," : "") +
                "neuronCount='" + neuronCount + '\'' + "," +
                "dendriteCount='" + dendriteCount + '\'' + "," +
                (receiverSynapse != null ? "receiverSynapse=" + receiverSynapse + "," : "") +
                "axonCount='" + axonCount + '\'' + "," +
                (senderSynapse != null ? "senderSynapse=" + senderSynapse + "," : "") +
                "receptorEnergy='" + receptorEnergy + '\'' + "," +
                "transmitterCount='" + transmitterCount + '\'' + "," +
                "transmitterNeedActivation='" + transmitterNeedActivation + '\'' + "," +
                '}';
    }
}
