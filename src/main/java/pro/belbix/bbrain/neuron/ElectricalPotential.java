package pro.belbix.bbrain.neuron;

import java.time.Instant;

/**
 * Created by v.belykh on 22.05.2018
 */
public class ElectricalPotential {

    private long id = 0;
    private Receptor receptor;
    private Transmitter transmitter;
    private Neuron neuron = null;
    private String name = null;
    private long potential = 0;
    private Instant initTime = null;
    private Long fromReceiver = null;
    private Long fromAxon = null;
    private Long fromDendrite = null;
    private Synapse synapse = null;

    public ElectricalPotential(Receptor receptor, Transmitter transmitter) {
        this.receptor = receptor;
        this.transmitter = transmitter;
        this.potential = receptor.activateTransmitter(transmitter);
        synapse = receptor.getSynapse();
        id = receptor.getReceiver().getDendrite().getNeuron().nextEpCount();
        fromReceiver = receptor.getReceiver().getId();
        fromDendrite = receptor.getReceiver().getDendrite().getId();
        neuron = receptor.getReceiver().getDendrite().getNeuron();
    }

    public ElectricalPotential(long potential) {
        this.potential = potential;
    }


    public long getPotential() {
        return potential;
    }

    public Instant getInitTime() {
        return initTime;
    }

    public void setInitTime(Instant initTime) {
        this.initTime = initTime;
    }

    public String getName() {
        if (name == null) name = neuron.getName() + "E" + id;
        return name;
    }

    public Long getFromReceiver() {
        return fromReceiver;
    }

    public void setFromReceiver(Long fromReceiver) {
        this.fromReceiver = fromReceiver;
    }

    public Long getFromAxon() {
        return fromAxon;
    }

    public void setFromAxon(Long fromAxon) {
        this.fromAxon = fromAxon;
    }

    public Long getFromDendrite() {
        return fromDendrite;
    }

    public void setFromDendrite(Long fromDendrite) {
        this.fromDendrite = fromDendrite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Neuron getNeuron() {
        return neuron;
    }

    public void setNeuron(Neuron neuron) {
        this.neuron = neuron;
    }

    public Synapse getSynapse() {
        return synapse;
    }

    public void setSynapse(Synapse synapse) {
        this.synapse = synapse;
    }

    @Override
    public String toString() {
        return "ElectricalPotential{" +
                (name != null ? "name='" + name + '\'' + "," : "") +
                "potential='" + potential + '\'' + "," +
                (initTime != null ? "initTime=" + initTime + "," : "") +
                '}';
    }

}
