package pro.belbix.bbrain.neuron;

import pro.belbix.bbrain.models.Id;
import pro.belbix.bbrain.models.ModelVariable;
import pro.belbix.bbrain.utils.Config;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by v.belykh on 25.04.2018
 */
public class Axon {
    // --------------- STATE FIELDS -----------------------
    @Id
    private long id = 0;
    @Id
    private Long layer_id;
    @Id
    private Long neuron_id;
    @ModelVariable
    private String name = null;
    @ModelVariable
    private long timeNeedToActivation = Config.axonDefaultTimeNeedToActivation; // аналог времени прохождения сигнала по аксону, зависит от миелинизации
    @ModelVariable
    private int bapResistGenerate = Config.axonDefaultBapResist;
    @ModelVariable
    private boolean wasActivate = false;
    @ModelVariable
    private float x = 0;
    @ModelVariable
    private float y = 0;
    @ModelVariable
    private float z = 0;

    //------------- PARENT ----------------------------------
    private Neuron neuron = null;

    //------------- CHILD -----------------------------------
    private List<Sender> senders = new CopyOnWriteArrayList<>();

    //------------- LOCAL VARIABLE --------------------------
    private boolean activateSpike = false;
    private Instant activationTime = null;
    private Instant lTime = null;
    private long wishCreateSender = 0;
    private long senderIdCount = 0;

    public Axon(Neuron neuron) {
        this.neuron = neuron;
        layer_id = neuron.getLayerId();
        neuron_id = neuron.getId();
        neuron.addAxon(this);
    }

    public synchronized ArrayList<Destination> processing() {

        if (neuron.getLayer().getProcessingTime() == null) {
            lTime = Instant.now();
        } else {
            lTime = neuron.getLayer().getProcessingTime();
        }
        if (activateSpike &&
                activationTime != null
                && timeNeedToActivation <= Duration.between(activationTime, lTime).toMillis()
                ) {
            ArrayList<Destination> destinations = sendSpike(new Spike());
            setActivateSpike(false);
            setActivationTime(null);
            return destinations;
        }
        return null;
    }

    /**
     * Этот метод - абстракция над прохождением спайка по аксонному каналу к синапсам
     */
    public ArrayList<Destination> sendSpike(Spike spike) {
        ArrayList<Destination> destinations = null;
        boolean sent = false;
        for (Sender sender : senders) {
            ArrayList<Destination> d = sender.createTransmitters(spike);
            if (d != null && !d.isEmpty()) {
                if (destinations == null) destinations = new ArrayList<>();
                sent = true;
                destinations.addAll(d);
            }
        }
        calculateWishes(true);
        return destinations;
    }

    private void calculateWishes(boolean increment){
        if(increment) {
            if (senders.size() < neuron.getLayer().getSenderMax()) {
                wishCreateSender += Config.wishCreateSenderIncr;
            }
        }
//        else {
//            wishCreateSender -= Config.wishCreateSenderIncr;
//        }
    }


    /**
     * Not sure about this...
     */
    public void createBAP() { //back-propagating action potential
        ElectricalPotential potential = new ElectricalPotential(bapResistGenerate);
        potential.setInitTime(lTime);
        potential.setId(neuron.nextEpCount());
        potential.setNeuron(neuron);
        potential.setFromAxon(id);
        neuron.getPotentials().add(potential);
    }


    public Neuron getNeuron() {
        return neuron;
    }

    public void setNeuron(Neuron neuron) {
        this.neuron = neuron;
    }

    public List<Sender> getSenders() {
        return senders;
    }

    public void addSender(Sender sender) {
        this.senders.add(sender);
    }

    public String getName() {
        if (name == null)
            name = neuron.getName() + "A" + id;
        return name;
    }

    public boolean isActivateSpike() {
        return activateSpike;
    }

    public void setActivateSpike(boolean activateSpike) {
        wasActivate = true;
        this.activateSpike = activateSpike;
    }

    public long nextSenderId(){
        return senderIdCount++;
    }

    public Instant getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(Instant activationTime) {
        this.activationTime = activationTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getLayer_id() {
        return layer_id;
    }

    public void setLayer_id(Long layer_id) {
        this.layer_id = layer_id;
    }

    public Long getNeuron_id() {
        return neuron_id;
    }

    public void setNeuron_id(Long neuron_id) {
        this.neuron_id = neuron_id;
    }

    public long getTimeNeedToActivation() {
        return timeNeedToActivation;
    }

    public void setTimeNeedToActivation(long timeNeedToActivation) {
        this.timeNeedToActivation = timeNeedToActivation;
    }

    public int getBapResistGenerate() {
        return bapResistGenerate;
    }

    public void setBapResistGenerate(int bapResistGenerate) {
        this.bapResistGenerate = bapResistGenerate;
    }

    public long getWishCreateSender() {
        return wishCreateSender;
    }

    public void setWishCreateSender(long wishCreateSender) {
        this.wishCreateSender = wishCreateSender;
    }

    public void setWasActivate(boolean wasActivate) {
        this.wasActivate = wasActivate;
    }

    public boolean isWasActivate() {
        return wasActivate;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "Axon{" +
                (name != null ? "name='" + name + '\'' + "," : "") +
                "bapResistGenerate='" + bapResistGenerate + '\'' + "," +
                '}';
    }
}
