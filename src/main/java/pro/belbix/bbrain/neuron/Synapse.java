package pro.belbix.bbrain.neuron;

/**
 * Created by v.belykh on 23.07.2018
 */
public class Synapse {


    public enum Type {
        ACTIVATOR, INHIBITOR, STIMULATION, PUNISH, PRINT
    }

    private Type type = null;
    private Long layerId = null;

    public Synapse(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Synapse{" +
                (type != null ? "type=" + type + "," : "") +
                (layerId != null ? "layerId=" + layerId + "," : "") +
                '}';
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Long getLayerId() {
        return layerId;
    }

    public void setLayerId(Long layerId) {
        this.layerId = layerId;
    }

}
