package pro.belbix.bbrain.neuron;

import pro.belbix.bbrain.models.Id;

/**
 * Created by v.belykh on 16.05.2018
 */
public class Receptor {
    // --------------- MODEL VARIABLE -----------------------
    @Id
    private long id = 0;
    @Id
    private Long layer_id;
    @Id
    private Long receiver_id;
    @Id
    private Long dendrite_id;
    @Id
    private Long neuron_id;
    private String name = null;
    private Double energy = null;
    private Synapse synapse = null;

    //------------- PARENT ----------------------------------
    private Receiver receiver = null;


    public Receptor(Receiver receiver) {
        setReceiver(receiver);
    }

    public boolean activation(Transmitter transmitter) {
        //TODO магнивевые пробки
        return synapse.getType().equals(transmitter.getSynapse().getType());
    }

    public synchronized void increaseEnergy(double value) {
        if (this.energy == 0) return;
        if (this.energy > (receiver.getDendrite().getNeuron().getLayer().getReceptorEnergy() * 10)) return;
        this.energy += value;
    }

    public synchronized void decreaseEnergy(double value) {
        if (this.energy == 0) return;
        this.energy -= value;
        if (this.energy < 0) this.energy = 0.0;
    }

    public long activateTransmitter(Transmitter transmitter) {
        double l = energy * (double) transmitter.getCount();
        switch (synapse.getType()) {
            case INHIBITOR:
                l = -Math.abs(l);
                break;
        }
        return Math.round(l);
    }

    public String getName() {
        if (name == null) name = receiver.getName() + "RP" + id;
        return name;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
        layer_id = receiver.getDendrite().getNeuron().getLayerId();
        neuron_id = receiver.getDendrite().getNeuron().getId();
        dendrite_id = receiver.getDendrite().getId();
        receiver_id = receiver.getId();
    }

    public void delete() {
        this.receiver.getReceptors().remove(this);
        this.receiver = null;
        layer_id = null;
        neuron_id = null;
        dendrite_id = null;
        receiver_id = null;
        synapse = null;
    }


    public Synapse getSynapse() {
        return synapse;
    }

    public void setSynapse(Synapse synapse) {
        this.synapse = synapse;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Receiver getReceiver() {
        return receiver;
    }


    public Long getLayer_id() {
        return layer_id;
    }

    public void setLayer_id(Long layer_id) {
        this.layer_id = layer_id;
    }

    public Long getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(Long receiver_id) {
        this.receiver_id = receiver_id;
    }

    public Long getDendrite_id() {
        return dendrite_id;
    }

    public void setDendrite_id(Long dendrite_id) {
        this.dendrite_id = dendrite_id;
    }

    public Long getNeuron_id() {
        return neuron_id;
    }

    public void setNeuron_id(Long neuron_id) {
        this.neuron_id = neuron_id;
    }

    @Override
    public String toString() {
        return "Receptor{" +
                (getName() != null ? "name='" + getName() + '\'' + "," : "") +
                "energy='" + energy + '\'' + "," +
                "synapse='" + synapse + '\'' + "," +
                '}';
    }
}
