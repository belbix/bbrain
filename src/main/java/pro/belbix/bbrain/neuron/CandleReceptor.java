package pro.belbix.bbrain.neuron;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.stock.Price;
import pro.belbix.bbrain.utils.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by v.belykh on 25.05.2018
 */
public class CandleReceptor {
    private static final Logger log = LogManager.getRootLogger();

    private Price price = null;
    private List<Receiver> receivers = new CopyOnWriteArrayList<>();
    private int transmitterCount = Config.transmitterDefaultCount;
    private Sender sender = null;

    public CandleReceptor() {
        Layer layer = new Layer();
        layer.setId(-1);
        Neuron neuron = new Neuron();
        neuron.setLayer(layer);
        neuron.setId(-1);
        Axon axon = new Axon(neuron);
        sender = new Sender(axon);
    }

    public void processing(){
        if (price == null) return;
        receiveCandle();
        price = null;
    }

    public void receiveCandle() {
        if (receivers.isEmpty()) return;
        Transmitter transmitter = createTransmitter();
        if (transmitter == null) return;

        log.trace("get for receivers " + receivers.size() + " " + price);
        for (Receiver receiver : receivers) {
            receiver.addTransmitter(transmitter);
        }
    }

    private Transmitter createTransmitter() {
        int count = calcTransmittersCount();
        if (count == 0) return null;
        Transmitter transmitter = new Transmitter();
        transmitter.setSynapse(new Synapse(Synapse.Type.ACTIVATOR));
        transmitter.setSender(sender);
        transmitter.setCount(count);
        log.trace("Candle:" + transmitter + " " + price);
        return transmitter;
    }

    private int calcTransmittersCount() {
        double d = (price.getValue() - price.getMinValue()) / (price.getMaxValue() - price.getMinValue());
        return (int) ((double) transmitterCount * d);
    }

    public static ArrayList<CandleReceptor> createCandleReceptors(int count) {
        ArrayList<CandleReceptor> candleReceptors = new ArrayList<>();
        log.info("Start createCandleReceptors");
        int i = 0;
        while (i < count) {
            CandleReceptor receptor = new CandleReceptor();
            candleReceptors.add(receptor);
            i++;
        }
        log.info("Create " + candleReceptors.size() + " candleReceptors");
        return candleReceptors;
    }

    public void addOutputReceiver(Receiver receiver) {
        receivers.add(receiver);
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
}
