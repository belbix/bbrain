package pro.belbix.bbrain;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.belbix.bbrain.neuron.Photoreceptor;
import pro.belbix.bbrain.utils.Config;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by v.belykh on 08.05.2018
 */
public class Eye implements Runnable{
    private static final Logger log = LogManager.getRootLogger();
    private int width = 0;
    private int height = 0;
    private boolean run = true;
    private BufferedImage img = null;
    private boolean saveVision = true;
    private boolean startNeurons = true;
    private HashMap<Integer, Photoreceptor> photoreceptors = null;

    private class Coordinate{
        int x;
        int y;
    }


    @Override
    public void run() {
        try {
            initEye();
            startSee();
        } catch (AWTException | IOException e) {
            e.printStackTrace();
            log.error("Eye main loop error", e);
        }
    }

    private void initEye(){
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        width = Config.eyeWidth;
        height = Config.eyeHeight;
        if (width > size.width) width = size.width;
        if (height > size.height) height = size.height;
        photoreceptors = Photoreceptor.createPhotoreceptors(width * height * 3);
    }

    private void startSee() throws AWTException, IOException {


        Rectangle rect = new Rectangle(0, 0, width, height);
        Robot robot = new Robot();

        while (run) {
            img = robot.createScreenCapture(rect);
            if (saveVision) {
                ImageIO.write(img, "BMP", new File("/bbrain/screens/screen.bmp"));
            }

            if (startNeurons){
                int[] colors = getRGBs();
                log.info("get rgb: " + Arrays.toString(colors));
                for (int i = 0; i < colors.length; i++){
                    photoreceptors.get(i).receiveColor(colors[i]);
                }
            }

            try {
                Thread.sleep(Config.eyeSleep);
            } catch (InterruptedException e) {}
        }
    }

    public int[] getRGBs(){
        if (img == null) return new int[0];
        int[] rgb = new int[width*height];
        rgb = img.getRGB(0, 0,width, height, rgb,0, width);
        int[] rgbParsed = new int[rgb.length * 3];
        int count = 0;
        for (int color: rgb){

            int[] parsedColor = parseRGB(color);
            rgbParsed[count * 3] = parsedColor[0];
            rgbParsed[count * 3 + 1] = parsedColor[1];
            rgbParsed[count * 3 + 2] = parsedColor[2];
            count++;
        }
        return rgbParsed;
    }

    private static int[] parseRGB(int rgb){
        int red = (rgb >> 16) & 0xFF;
        int green = (rgb >> 8) & 0xFF;
        int blue = rgb & 0xFF;
        int[] result = new int[3];
        result[0] = red;
        result[1] = green;
        result[2] = blue;
        return result;
    }

    public void waitImg(){
        while (run){
            if (img != null) return;
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {}
        }
    }

    public HashMap<Integer, Photoreceptor> getPhotoreceptors() {
        return photoreceptors;
    }

    public void stop(){
        run = false;
    }

}
