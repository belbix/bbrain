import pro.belbix.bbrain.db.DBLayer;
import pro.belbix.bbrain.stock.StockData;

import java.sql.SQLException;
import java.time.LocalDateTime;

import static java.time.temporal.ChronoUnit.DAYS;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * Created by v.belykh on 25.05.2018
 */
public class TestDB {

//    @Test
    public void getCandlesTest() {
        DBLayer dbLayer = new DBLayer();
        StockData stockData = new StockData(dbLayer);
        try {
            dbLayer.openConnection();
            LocalDateTime fromDate = LocalDateTime.now().minus(30, DAYS);
            assertNotNull(stockData.getCandles("ib", "EUR", 5, fromDate, 100));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("getCandlesTest successful");
    }

}
