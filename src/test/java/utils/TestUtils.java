package utils;

import pro.belbix.bbrain.neuron.*;
import pro.belbix.bbrain.processing.ProcessingCenter;

import java.time.Instant;

import static pro.belbix.bbrain.neuron.Synapse.Type.ACTIVATOR;

public class TestUtils {

    public static void printNeuronStats(Neuron neuron){
        long membraneResist = neuron.getMembraneResistance();
        Instant lastSpike = neuron.getLastSpike();
        long amountPotential = neuron.getAmountPotentials();
        long reffractoryResist = neuron.getRefractoryResist();

        System.out.println(neuron.getName() + ":\n"
        +"membraneResist: " + membraneResist + "\n"
        +"lastSpike: " + lastSpike + "\n"
        +"amountPotential: " + amountPotential + "\n"
        +"reffractoryResist: " + reffractoryResist + "\n"
        );
    }

    public static void addTransmitterToNeuron(Neuron neuron, Transmitter transmitter, int count){
        transmitter.setCount(count);
        neuron.getDendrits().get(0).getReceivers().get(0).addTransmitter(transmitter);
    }

    public static Transmitter createTransmitter(){
        Transmitter transmitter = new Transmitter();
        transmitter.setSynapse(new Synapse(ACTIVATOR));
        transmitter.setCount(1);
        return transmitter;
    }

    public static void simpleHandleNeuron(Neuron neuron){
        for(Dendrite dendrite: neuron.getDendrits()) {
            for (Receiver receiver : dendrite.getReceivers()) {
                receiver.processing();
            }
        }

        neuron.processing();

        for (Axon axon : neuron.getAxons()) {
            axon.processing();
        }
        ProcessingCenter.addNeuronForVisual(neuron);
    }

}
