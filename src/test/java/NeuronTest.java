import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized;
import pro.belbix.bbrain.build.MindBox;
import pro.belbix.bbrain.build.NeuronFactory;
import pro.belbix.bbrain.neuron.*;
import pro.belbix.bbrain.utils.Config;
import pro.belbix.bbrain.visual.BBVisual;
import utils.TestUtils;

import java.util.Arrays;
import java.util.Collection;

import static pro.belbix.bbrain.neuron.Synapse.Type.ACTIVATOR;
import static pro.belbix.bbrain.neuron.Synapse.Type.INHIBITOR;
import static utils.TestUtils.simpleHandleNeuron;

/**
 * Created by v.belykh on 10/12/2018
 */
//@RunWith(Parameterized.class)
public class NeuronTest {
    private static final int ITERATIONS = 100;
    private static final int DELAY = 1000;
    private static final boolean VISUAL = true;
    private static BBVisual bbVisual = null;

    private int[] input;

//    public NeuronTest(int[] input) {
//        this.input = input;
//    }

    private Neuron neuronX1;
    private Neuron neuronX2;
    private Neuron hide1;
    private Neuron hide2;
    private Neuron hide3;
    private Neuron neuronOut;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return
                Arrays.asList(
                        new Object[][]{
                                {new int[]{0, 0, 0}}
                                , {new int[]{1, 1, 0}}
                                , {new int[]{0, 1, 1}}
                                , {new int[]{1, 0, 1}}
                        });
    }


    @Before
    public void init(){
        if (VISUAL) {
            bbVisual = new BBVisual();
            new Thread(bbVisual).start();
        }
        createNeurons();
        learnNeurons();
    }

    @After
    public void close() {
        if (bbVisual != null) bbVisual.close();
    }

    @Test
    public void xorTest() {
        if(input == null) return;
        if(input[0] == 1){
            TestUtils.addTransmitterToNeuron(neuronX1, TestUtils.createTransmitter(), 100);
        }
        if(input[1] == 1){
            TestUtils.addTransmitterToNeuron(neuronX2, TestUtils.createTransmitter(), 100);
        }
        simpleHandleNeuron(neuronX1);
        simpleHandleNeuron(neuronX2);
        simpleHandleNeuron(hide1);
        simpleHandleNeuron(hide2);
        simpleHandleNeuron(hide3);
        simpleHandleNeuron(neuronOut);
        Assert.assertEquals((input[0] != input[1]), neuronOut.getAxons().get(0).isWasActivate());
    }

    private void learnNeurons(){
        for (int i = 0; i < ITERATIONS; i++) {
            int[] xor = setupTransmitters();
            simpleHandleNeuron(neuronX1);
            simpleHandleNeuron(neuronX2);
            simpleHandleNeuron(hide1);
            simpleHandleNeuron(hide2);
            simpleHandleNeuron(hide3);
            simpleHandleNeuron(neuronOut);

            System.out.println("xor:" + Arrays.toString(xor) + "(" + (xor[0] != xor[1]) + ")"
                    + " result:" + neuronOut.getAxons().get(0).isWasActivate());

            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private int[] setupTransmitters() {
        double random1 = Math.random();
        double random2 = Math.random();
        int x1 = 0;
        int x2 = 0;
        if (random1 > 0.5) {
            x1 = 1;
            TestUtils.addTransmitterToNeuron(neuronX1, TestUtils.createTransmitter(), 100);
        }
        if (random2 > 0.5) {
            x2 = 1;
            TestUtils.addTransmitterToNeuron(neuronX2, TestUtils.createTransmitter(), 100);
        }
        return new int[]{x1, x2};
    }


    private void createNeurons() {
        MindBox mindBox = new MindBox();

        Layer layerX = new Layer();
        layerX.setId(0);
        layerX.setNeuronCount(2);
        //--------------- X1 ----------------------------------------
        neuronX1 = NeuronFactory.createEmptyNeuron(0, layerX);
        Axon axonX1 = new Axon(neuronX1);
        Sender sender1x1 = NeuronFactory.createEmptySender(0, axonX1);
        NeuronFactory.createTransmitter(0, sender1x1, new Synapse(INHIBITOR));
        Sender sender2x1 = NeuronFactory.createEmptySender(1, axonX1);
        NeuronFactory.createTransmitter(0, sender2x1, new Synapse(ACTIVATOR));
        Sender sender3x1 = NeuronFactory.createEmptySender(2, axonX1);
        NeuronFactory.createTransmitter(0, sender3x1, new Synapse(ACTIVATOR));

        NeuronFactory.createReceptor(0,
                NeuronFactory.createReceiver(0, new Dendrite(neuronX1)),
                new Synapse(ACTIVATOR), Config.receiverReceptorDefaultEnergy);
        NeuronFactory.calcCoordinates(neuronX1, mindBox);

        //--------------- X2 ----------------------------------------
        neuronX2 = NeuronFactory.createEmptyNeuron(1, layerX);
        Axon axonX2 = new Axon(neuronX2);
        Sender sender1x2 = NeuronFactory.createEmptySender(0, axonX2);
        NeuronFactory.createTransmitter(0, sender1x2, new Synapse(ACTIVATOR));
        Sender sender2x2 = NeuronFactory.createEmptySender(1, axonX2);
        NeuronFactory.createTransmitter(0, sender2x2, new Synapse(INHIBITOR));
        Sender sender3x2 = NeuronFactory.createEmptySender(2, axonX2);
        NeuronFactory.createTransmitter(0, sender3x2, new Synapse(ACTIVATOR));

        NeuronFactory.createReceptor(0,
                NeuronFactory.createReceiver(0, new Dendrite(neuronX2)),
                new Synapse(ACTIVATOR), Config.receiverReceptorDefaultEnergy);
        NeuronFactory.calcCoordinates(neuronX2, mindBox);

        //--------------- HIDE ----------------------------------------
        Layer layerHide = new Layer();
        layerHide.setId(1);
        layerHide.setNeuronCount(3);

        hide1 = NeuronFactory.createEmptyNeuron(0, layerHide);
        Axon axonHide1 = new Axon(hide1);
        Sender senderHide1 = NeuronFactory.createEmptySender(0, axonHide1);
        NeuronFactory.createTransmitter(0, senderHide1, new Synapse(ACTIVATOR));
        Dendrite dendriteHide1 = new Dendrite(hide1);
        Receiver receiver1Hide1 = NeuronFactory.createReceiver(0, dendriteHide1);
        NeuronFactory.createReceptor(0, receiver1Hide1, new Synapse(INHIBITOR), Config.receiverReceptorDefaultEnergy);
        Receiver receiver2Hide1 = NeuronFactory.createReceiver(1, dendriteHide1);
        NeuronFactory.createReceptor(0, receiver2Hide1, new Synapse(ACTIVATOR), Config.receiverReceptorDefaultEnergy);
        NeuronFactory.calcCoordinates(hide1, mindBox);


        hide2 = NeuronFactory.createEmptyNeuron(1, layerHide);
        Axon axonHide2 = new Axon(hide2);
        Sender senderHide2 = NeuronFactory.createEmptySender(0, axonHide2);
        NeuronFactory.createTransmitter(0, senderHide2, new Synapse(ACTIVATOR));
        Dendrite dendriteHide2 = new Dendrite(hide2);
        Receiver receiver1Hide2 = NeuronFactory.createReceiver(0, dendriteHide2);
        NeuronFactory.createReceptor(0, receiver1Hide2, new Synapse(ACTIVATOR), Config.receiverReceptorDefaultEnergy);
        Receiver receiver2Hide2 = NeuronFactory.createReceiver(1, dendriteHide2);
        NeuronFactory.createReceptor(0, receiver2Hide2, new Synapse(INHIBITOR), Config.receiverReceptorDefaultEnergy);
        NeuronFactory.calcCoordinates(hide2, mindBox);


        hide3 = NeuronFactory.createEmptyNeuron(2, layerHide);
        Axon axonHide3 = new Axon(hide3);
        Sender senderHide3 = NeuronFactory.createEmptySender(0, axonHide3);
        NeuronFactory.createTransmitter(0, senderHide3, new Synapse(INHIBITOR));
        Dendrite dendriteHide3 = new Dendrite(hide3);
        Receiver receiver1Hide3 = NeuronFactory.createReceiver(0, dendriteHide3);
        NeuronFactory.createReceptor(0, receiver1Hide3, new Synapse(ACTIVATOR), Config.receiverReceptorDefaultEnergy);
        Receiver receiver2Hide3 = NeuronFactory.createReceiver(1, dendriteHide3);
        NeuronFactory.createReceptor(0, receiver2Hide3, new Synapse(ACTIVATOR), Config.receiverReceptorDefaultEnergy);
        NeuronFactory.calcCoordinates(hide3, mindBox);

        //--------------- OUT ----------------------------------------

        Layer layer = new Layer();
        layer.setId(2);
        layer.setNeuronCount(1);
        neuronOut = NeuronFactory.createEmptyNeuron(0, layer);
        Axon axonOut = new Axon(neuronOut);
        Sender senderOut = NeuronFactory.createEmptySender(0, axonOut);
        NeuronFactory.createTransmitter(0, senderOut, new Synapse(ACTIVATOR));

        Dendrite dendrite1Out = new Dendrite(neuronOut);
        Receiver receiver1Out = NeuronFactory.createReceiver(0, dendrite1Out);
        NeuronFactory.createReceptor(0, receiver1Out, new Synapse(ACTIVATOR), Config.receiverReceptorDefaultEnergy);
        Receiver receiver2Out = NeuronFactory.createReceiver(1, dendrite1Out);
        NeuronFactory.createReceptor(0, receiver2Out, new Synapse(ACTIVATOR), Config.receiverReceptorDefaultEnergy);
        Receiver receiver3Out = NeuronFactory.createReceiver(2, dendrite1Out);
        NeuronFactory.createReceptor(0, receiver3Out, new Synapse(INHIBITOR), Config.receiverReceptorDefaultEnergy);
        NeuronFactory.calcCoordinates(neuronOut, mindBox);


        //--------------------- CONNECT ----------------------------------

        sender1x1.setReceiver(receiver1Hide1);
        sender2x1.setReceiver(receiver1Hide2);
        sender3x1.setReceiver(receiver1Hide3);

        sender1x2.setReceiver(receiver2Hide1);
        sender2x2.setReceiver(receiver2Hide2);
        sender3x2.setReceiver(receiver2Hide3);

        senderHide1.setReceiver(receiver1Out);
        senderHide2.setReceiver(receiver2Out);
        senderHide3.setReceiver(receiver3Out);

        mindBox.clearCoordinates();
        NeuronFactory.calcCoordinates(neuronX1, mindBox);
        NeuronFactory.calcCoordinates(neuronX2, mindBox);
        NeuronFactory.calcCoordinates(hide1, mindBox);
        NeuronFactory.calcCoordinates(hide2, mindBox);
        NeuronFactory.calcCoordinates(hide3, mindBox);
        NeuronFactory.calcCoordinates(neuronOut, mindBox);
    }

}
