create table axons
(
  id int not null,
  layer_id int not null,
  neuron_id int not null,
  name varchar(100) null,
  resist int null,
  delay int null,
  bap_resist int null,
  active tinyint null,
  primary key (id, layer_id, neuron_id)
)
;

create index axons_neuron_id_layer_id_fk
  on axons (neuron_id, layer_id)
;

create index fneuron_idx
  on axons (layer_id, neuron_id)
;

create table dendrites
(
  id int not null,
  layer_id int not null,
  neuron_id int not null,
  name varchar(100) null,
  primary key (id, layer_id, neuron_id)
)
;

create table eps
(
  id int not null,
  layer_id int not null,
  neuron_id int not null,
  name varchar(100) null,
  potential int null,
  init_time datetime(6) null,
  receiver_id int null,
  axon_id int null,
  dendrite_id int null,
  primary key (id, layer_id, neuron_id)
)
;

create index eps_neuron_id_layer_id_fk
  on eps (neuron_id, layer_id)
;

create index neuron_id_index
  on eps (neuron_id)
;

create table layers
(
  id int null,
  name varchar(100) null,
  type int null,
  ptime datetime(6) null,
  ncount int null,
  dcount int null,
  rvcount int null,
  acount int null,
  scount int null,
  renergy int null,
  tcount int null,
  tna int null,
  constraint layers_id_pk
  unique (id)
)
;

create table neurons
(
  id int not null,
  layer_id int not null,
  name varchar(100) null,
  resist int null,
  last_spike datetime(6) null,
  amountep int null,
  refres int null,
  primary key (id, layer_id)
)
;

create table receivers
(
  id int not null,
  layer_id int not null,
  dendrite_id int not null,
  neuron_id int not null,
  name varchar(100) null,
  whence int null,
  primary key (id, layer_id, dendrite_id, neuron_id)
)
;

create index receivers_dendrites_id_layer_id_neuron_id_fk
  on receivers (dendrite_id, layer_id, neuron_id)
;

create index receivers_neuron_id_layer_id_fk
  on receivers (neuron_id, layer_id)
;

create table receptors
(
  id int not null,
  layer_id int not null,
  receiver_id int not null,
  dendrite_id int not null,
  neuron_id int not null,
  name varchar(100) null,
  type int null,
  distance int null,
  energy int null,
  primary key (id, layer_id, receiver_id, dendrite_id, neuron_id)
)
;

create index receptors_dendrites_id_layer_id_neuron_id_fk
  on receptors (dendrite_id, layer_id, neuron_id)
;

create index receptors_neuron_id_layer_id_fk
  on receptors (neuron_id, layer_id)
;

create index receptors_receivers_id_layer_id_dendrite_id_neuron_id_fk
  on receptors (receiver_id, layer_id, dendrite_id, neuron_id)
;

create table senders
(
  id int not null,
  layer_id int not null,
  axon_id int not null,
  neuron_id int not null,
  name varchar(100) null,
  destination int null,
  receiver_id int null,
  rlayer_id int null,
  rdendrite_id int null,
  rneuron_id int null,
  primary key (id, layer_id, axon_id, neuron_id)
)
;

create index senders_axons_id_layer_id_neuron_id_fk
  on senders (axon_id, layer_id, neuron_id)
;

create index senders_neuron_id_layer_id_fk
  on senders (neuron_id, layer_id)
;

create index senders_receivers_id_layer_id_dendrite_id_neuron_id_fk
  on senders (receiver_id, rlayer_id, rdendrite_id, rneuron_id)
;

create table transmitters
(
  id int not null,
  layer_id int not null,
  sender_id int not null,
  axon_id int not null,
  neuron_id int not null,
  name varchar(100) null,
  type int null,
  distance int null,
  count int null,
  power int null,
  activation_limit int null,
  primary key (id, layer_id, sender_id, axon_id, neuron_id)
)
;

create index transmitters_axons_id_layer_id_neuron_id_fk
  on transmitters (axon_id, layer_id, neuron_id)
;

create index transmitters_neuron_id_layer_id_fk
  on transmitters (neuron_id, layer_id)
;

create index transmitters_senders_id_layer_id_axon_id_neuron_id_fk
  on transmitters (sender_id, layer_id, axon_id, neuron_id)
;

